package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class FilterActivity extends Activity {
	
	private static String webCallApi;
	private Spinner countrySpinner;
	private Spinner typeSpinner;
	private Spinner ratingSpinner;
	private Spinner priceSpinner;
	private Button getResultButton;
	private TextView header;
	private TextView countryLabel;
	private TextView typeLabel;
	private TextView ratingLabel;
	private TextView priceLabel;
	private ProgressDialog pd;
	private ArrayList<String> countryCodeList;
	private ArrayList<String> typeCodeList;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
        View viewToLoad = LayoutInflater.from(this.getParent()).inflate(R.layout.filter_page, null);
        setContentView(viewToLoad);
        
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
        countryLabel = (TextView) findViewById(R.id.country_label);
        typeLabel = (TextView) findViewById(R.id.type_label);
        ratingLabel = (TextView) findViewById(R.id.rating_label);
        priceLabel = (TextView) findViewById(R.id.price_label);
        countryLabel.setTypeface(font1);
        typeLabel.setTypeface(font1);
        ratingLabel.setTypeface(font1);
        priceLabel.setTypeface(font1);
        
        countrySpinner = (Spinner) findViewById(R.id.country_spinner);
    	typeSpinner = (Spinner) findViewById(R.id.type_spinner);
    	ratingSpinner = (Spinner) findViewById(R.id.rating_spinner);
    	priceSpinner = (Spinner) findViewById(R.id.price_spinner);
    	getResultButton = (Button) findViewById(R.id.filter_result);
    	Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    getResultButton.setTypeface(font2); 
    	
    	// header
	    header = (TextView) findViewById(R.id.filter_page_header);  
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font); 
	    
	    
    	
    	getResultButton.setOnClickListener(new View.OnClickListener() { 
    		public void onClick(View v) {
                getSearchResult(v);
            }
    	});
        
        ArrayList<String> countryList = new ArrayList<String>();
        countryCodeList = new ArrayList<String>();
        ArrayList<String> typeList = new ArrayList<String>();
        typeCodeList = new ArrayList<String>();
        ArrayList<String> ratingList = new ArrayList<String>();
        ArrayList<String> priceList = new ArrayList<String>();
        
        webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine_filter.php?mode=M";
        // XPath initialization
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    String expression = "//item";
	    InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		NodeList nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Get Wine Type
		typeList.add("ALL");
		typeCodeList.add("");
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("config_value")) {
	    			typeList.add(childNodes.item(y).getTextContent().toUpperCase());
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("config_key")) {
	    			typeCodeList.add(childNodes.item(y).getTextContent().toUpperCase());
	    		}
	    	}
	    }
	    
	    // Get Price Range - hardcoded part
	    priceList.add("ALL");
	    priceList.add("< $20");
	    priceList.add("$20 - $40");
	    priceList.add("$40 - $60");
	    priceList.add("$60 - $80");
	    priceList.add("$80 - $100");
	    priceList.add("$100 >");
//	    expression = "//Range";
//	    inputSource = null;
//		try {
//			inputSource = new InputSource(new URL(webCallApi).openStream());
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		// XPath Query
//		nodes = null;
//	    try {
//			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
//		} catch (XPathExpressionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    
//	    for (int x = 0; x < nodes.getLength(); x++) {
//	    	Node node = nodes.item(x);
//	    	NodeList childNodes = node.getChildNodes();
//	    	String priceRange = "";
//	    	for (int y = 0; y < childNodes.getLength(); y++) {
//	    		// lower range
//	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("Min")) {
//	    			// lowest range
//	    			if (childNodes.item(y).getTextContent().equalsIgnoreCase("x")) {
//	    				// do nothing
//	    			} else { // middle range
//	    				priceRange = "$"+childNodes.item(y).getTextContent();
//	    			}
//	    		}
//	    		// higher range
//	    		else if (childNodes.item(y).getNodeName().equalsIgnoreCase("Max")) {
//	    			// highest range
//	    			if (childNodes.item(y).getTextContent().equalsIgnoreCase("x")) {
//	    				priceRange += " AND ABOVE";
//	    			} else {
//	    				if (priceRange.equalsIgnoreCase("")) { // with lowest range
//	    					priceRange += "$"+childNodes.item(y).getTextContent()+" AND BELOW";
//	    				} else { // middle range
//	    					priceRange += "-$"+childNodes.item(y).getTextContent();
//	    				}
//	    			}
//	    		}
//	    	}
//	    	priceList.add(priceRange);
//	    }
	    
	    // Get Rating
	    ratingList.add("ALL");
	    ratingList.add("1/5");
	    ratingList.add("2/5");
	    ratingList.add("3/5");
	    ratingList.add("4/5");
	    ratingList.add("5/5");
//	    expression = "//Rating";
//	    inputSource = null;
//		try {
//			inputSource = new InputSource(new URL(webCallApi).openStream());
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		// XPath Query
//		nodes = null;
//	    try {
//			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
//		} catch (XPathExpressionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    
//	    for (int x = 0; x < nodes.getLength(); x++) {
//	    	Node node = nodes.item(x);
//	    	NodeList childNodes = node.getChildNodes();
//	    	int highestRange = 0;
//	    	for (int y = 0; y < childNodes.getLength(); y++) {
//	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("MaxStar")) {
//	    			highestRange = Integer.parseInt(childNodes.item(y).getTextContent());
//	    		}
//	    	}
//	    	for (int a = 1; a <= highestRange; a++) {
//	    		ratingList.add(String.valueOf(a)+"/"+highestRange);
//	    	}
//	    }
	    
	    // Get Country
	    webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_country.php?mode=M";
	    expression = "//item";
	    inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		countryList.add("ALL");
		countryCodeList.add("");
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("country_name")) {
	    			countryList.add(childNodes.item(y).getTextContent().toUpperCase());
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("country_code")) {
	    			countryCodeList.add(childNodes.item(y).getTextContent().toUpperCase());
	    		}
	    	}
	    }
    	
    	ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countryList);
    	countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	countrySpinner.setAdapter(countryAdapter);
    	
    	ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, typeList);
    	typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	typeSpinner.setAdapter(typeAdapter);
    	
    	ArrayAdapter<String> ratingAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ratingList);
    	ratingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	ratingSpinner.setAdapter(ratingAdapter);
    	
    	ArrayAdapter<String> priceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, priceList);
    	priceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	priceSpinner.setAdapter(priceAdapter);
        
    }
	
	public void getSearchResult(View view) {
		
		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				pd = ProgressDialog.show(getParent(), "", "Loading..");
				runOnUiThread(new Runnable(){
					public void run() {
						// Trigger the next intent and pass selected param
						Intent newIntent = new Intent(getParent(), SearchResultActivity.class);

						// for country, get the country code, e.g. AU is for Australia
						newIntent.putExtra("selectedCountry", countryCodeList.get(countrySpinner.getSelectedItemPosition()));
						
						// for type, get the type code, e.g. R is for Red Wine
						newIntent.putExtra("selectedType", typeCodeList.get(typeSpinner.getSelectedItemPosition()));
						
						// Split rating to get single digit
						String rating = String.valueOf(ratingSpinner.getSelectedItem());
						if (rating.equalsIgnoreCase("ALL") == true)
							newIntent.putExtra("selectedRating", "");
						else 
							newIntent.putExtra("selectedRating", rating.split("/")[0]);
						
						// get price range
						String priceRange = String.valueOf(priceSpinner.getSelectedItem());
						if (priceRange.equalsIgnoreCase("ALL")) {
							newIntent.putExtra("selectedPrice", "");
						} else if (priceRange.equalsIgnoreCase("< $20")) {
							newIntent.putExtra("selectedPrice", "0-20");
						} else if (priceRange.equalsIgnoreCase("$20 - $40")) {
							newIntent.putExtra("selectedPrice", "20-40");
						} else if (priceRange.equalsIgnoreCase("$40 - $60")) {
							newIntent.putExtra("selectedPrice", "40-60");
						} else if (priceRange.equalsIgnoreCase("$60 - $80")) {
							newIntent.putExtra("selectedPrice", "60-80");
						} else if (priceRange.equalsIgnoreCase("$80 - $100")) {
							newIntent.putExtra("selectedPrice", "80-100");
						} else {
							newIntent.putExtra("selectedPrice", "100-9999");
						}
						
						// Create the view using ThirdTabGroup's LocalActivityManager
				        View v = ThirdTabGroup.group.getLocalActivityManager()
				        .startActivity("SearchResultActivity", newIntent
				        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
				        .getDecorView();

				        // Again, replace the view
				        ThirdTabGroup.group.replaceView(v);
				        pd.dismiss();
					}
				}); 
				Looper.loop();
			}
		}).start();
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}