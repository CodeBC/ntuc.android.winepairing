package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class OrderConfirmationActivity extends Activity {

	private TextView fullName;
	private TextView email;
	private TextView mobile;
	private TextView address;
	private TextView wineOrder1, wineOrder2, wineOrder3, wineOrder4, wineOrder5;
	private TextView total1, total2, total3, total4, total5;
	private TextView orderLabel1, orderLabel2, orderLabel3, orderLabel4, orderLabel5;
	private TextView totalLabel1, totalLabel2, totalLabel3, totalLabel4, totalLabel5;
	private TextView price;
	private TextView saving;
	private TextView totalPrice;
	private Button confirm;
	private String winePrice1, winePrice2, winePrice3, winePrice4, winePrice5;
	private String clubSaving; 
	private TextView header;
	private TextView clubSavingLabel;
	private String selectedID1, selectedID2, selectedID3, selectedID4, selectedID5;
	private String webCallApi;
	private DecimalFormat decForm;
	private double discount;
	private String clubNumber;
	private String submitAPI;
	private String _firstName, _surName, _email, _mobile, _address, _wineClub, _wineOrder1, _wineOrder2, _wineOrder3, _wineOrder4, _wineOrder5, _wineName1, _wineName2, _wineName3, _wineName4, _wineName5, _quantity1, _quantity2, _quantity3, _quantity4, _quantity5, _estPrice, _clubSave, _total;
	private ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		setContentView(R.layout.order_confirm);
		//header 
		header = (TextView) findViewById(R.id.order_confirm_header);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font); 

		Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");
		Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");

		confirm = (Button) findViewById(R.id.confirm_btn);
		confirm.setTypeface(font2);

		decForm = new DecimalFormat("#.##");
		selectedID1 = this.getIntent().getStringExtra("selectedID1");
		selectedID2 = this.getIntent().getStringExtra("selectedID2");
		selectedID3 = this.getIntent().getStringExtra("selectedID3");
		selectedID4 = this.getIntent().getStringExtra("selectedID4");
		selectedID5 = this.getIntent().getStringExtra("selectedID5");

		// Wine Order label and Total Order Label
		orderLabel1 = (TextView) findViewById(R.id.confirm_order_label1);
		orderLabel2 = (TextView) findViewById(R.id.confirm_order_label2);
		orderLabel3 = (TextView) findViewById(R.id.confirm_order_label3);
		orderLabel4 = (TextView) findViewById(R.id.confirm_order_label4);
		orderLabel5 = (TextView) findViewById(R.id.confirm_order_label5);

		totalLabel1 = (TextView) findViewById(R.id.total_label1);
		totalLabel2 = (TextView) findViewById(R.id.total_label2);
		totalLabel3 = (TextView) findViewById(R.id.total_label3);
		totalLabel4 = (TextView) findViewById(R.id.total_label4);
		totalLabel5 = (TextView) findViewById(R.id.total_label5);

		clubSavingLabel = (TextView) findViewById(R.id.club_saving);
		clubSavingLabel.setTypeface(null, Typeface.BOLD);
		clubSavingLabel.setText(Html.fromHtml("Just Wine <br />Club Saving"));
		fullName = (TextView) findViewById(R.id.confirm_fullname);
		fullName.setTypeface(font1);
		email = (TextView) findViewById(R.id.confirm_email);
		email.setTypeface(font1);
		mobile = (TextView) findViewById(R.id.confirm_mobile);
		mobile.setTypeface(font1);
		address = (TextView) findViewById(R.id.confirm_address);
		address.setTypeface(font1);
		wineOrder1 = (TextView) findViewById(R.id.wine_order1);
		wineOrder1.setTypeface(font1);
		wineOrder2 = (TextView) findViewById(R.id.wine_order2);
		wineOrder2.setTypeface(font1);
		wineOrder3 = (TextView) findViewById(R.id.wine_order3);
		wineOrder3.setTypeface(font1);
		wineOrder4 = (TextView) findViewById(R.id.wine_order4);
		wineOrder4.setTypeface(font1);
		wineOrder5 = (TextView) findViewById(R.id.wine_order5);
		wineOrder5.setTypeface(font1);
		total1 = (TextView) findViewById(R.id.total1);
		total1.setTypeface(font1);
		total2 = (TextView) findViewById(R.id.total2);
		total2.setTypeface(font1);
		total3 = (TextView) findViewById(R.id.total3);
		total3.setTypeface(font1);
		total4 = (TextView) findViewById(R.id.total4);
		total4.setTypeface(font1);
		total5 = (TextView) findViewById(R.id.total5);
		total5.setTypeface(font1);

		clubNumber = this.getIntent().getStringExtra("clubNumber");
		String estPrice1 = "0";
		String estPrice2 = "0";
		String estPrice3 = "0";
		String estPrice4 = "0";
		String estPrice5 = "0";

		price = (TextView) findViewById(R.id.confirm_price);
		price.setTypeface(font1);
		saving = (TextView) findViewById(R.id.confirm_saving);
		saving.setTypeface(font1);
		totalPrice = (TextView) findViewById(R.id.confirm_total_price);
		totalPrice.setTypeface(font1);

		if (clubNumber != null && clubNumber.compareToIgnoreCase("") != 0) {
			discount = 0.08;
		} else {
			discount = 0;
		}

		XPath xpath = null;
		String expression = null;
		InputSource inputSource = null;
		NodeList nodes = null;

		if (selectedID1 != null && !selectedID1.equalsIgnoreCase("")) {
			// order 1 
			webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id="+selectedID1;
			// XPath initialization
			xpath = XPathFactory.newInstance().newXPath();
			expression = "//item";
			try {
				inputSource = new InputSource(new URL(webCallApi).openStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// XPath Query
			try {
				nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Loop through NodeList to store data into arrays
			for (int x = 0; x < nodes.getLength(); x++) {
				Node node = nodes.item(x);
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_price")) {
						winePrice1 = childNodes.item(y).getTextContent();
					}
				}
			}
			wineOrder1.setText(this.getIntent().getStringExtra("wineOrder1"));
			total1.setText(this.getIntent().getStringExtra("total1")+" bottles");
			estPrice1 = decForm.format(Double.valueOf(winePrice1) * Integer.parseInt(this.getIntent().getStringExtra("total1")));
		}

		if (selectedID2 != null && !selectedID2.equalsIgnoreCase("")) {
			// order 2
			webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id="+selectedID2;
			// XPath initialization
			xpath = XPathFactory.newInstance().newXPath();
			expression = "//item";
			inputSource = null;
			try {
				inputSource = new InputSource(new URL(webCallApi).openStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// XPath Query
			nodes = null;
			try {
				nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Loop through NodeList to store data into arrays
			for (int x = 0; x < nodes.getLength(); x++) {
				Node node = nodes.item(x);
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_price")) {
						winePrice2 = childNodes.item(y).getTextContent();
					}
				}
			}

			wineOrder2.setText(this.getIntent().getStringExtra("wineOrder2"));
			total2.setText(this.getIntent().getStringExtra("total2")+" bottles");
			estPrice2 = decForm.format(Double.valueOf(winePrice2) * Integer.parseInt(this.getIntent().getStringExtra("total2")));
		}

		if (selectedID3 != null && !selectedID3.equalsIgnoreCase("")) {
			// order 3
			webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id="+selectedID3;
			// XPath initialization
			xpath = XPathFactory.newInstance().newXPath();
			expression = "//item";
			inputSource = null;
			try {
				inputSource = new InputSource(new URL(webCallApi).openStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// XPath Query
			nodes = null;
			try {
				nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Loop through NodeList to store data into arrays
			for (int x = 0; x < nodes.getLength(); x++) {
				Node node = nodes.item(x);
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_price")) {
						winePrice3 = childNodes.item(y).getTextContent();
					}
				}
			}

			wineOrder3.setText(this.getIntent().getStringExtra("wineOrder3"));
			total3.setText(this.getIntent().getStringExtra("total3")+" bottles");
			estPrice3 = decForm.format(Double.valueOf(winePrice3) * Integer.parseInt(this.getIntent().getStringExtra("total3")));
		}

		if (selectedID4 != null && !selectedID4.equalsIgnoreCase("")) {
			// order 4
			webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id="+selectedID4;
			// XPath initialization
			xpath = XPathFactory.newInstance().newXPath();
			expression = "//item";
			inputSource = null;
			try {
				inputSource = new InputSource(new URL(webCallApi).openStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// XPath Query
			nodes = null;
			try {
				nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Loop through NodeList to store data into arrays
			for (int x = 0; x < nodes.getLength(); x++) {
				Node node = nodes.item(x);
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_price")) {
						winePrice4 = childNodes.item(y).getTextContent();
					}
				}
			}

			wineOrder4.setText(this.getIntent().getStringExtra("wineOrder4"));
			total4.setText(this.getIntent().getStringExtra("total4")+" bottles");
			estPrice4 = decForm.format(Double.valueOf(winePrice4) * Integer.parseInt(this.getIntent().getStringExtra("total4")));
		}

		if (selectedID5 != null && !selectedID5.equalsIgnoreCase("")) {
			// order 5
			webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id="+selectedID5;
			// XPath initialization
			xpath = XPathFactory.newInstance().newXPath();
			expression = "//item";
			inputSource = null;
			try {
				inputSource = new InputSource(new URL(webCallApi).openStream());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// XPath Query
			nodes = null;
			try {
				nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Loop through NodeList to store data into arrays
			for (int x = 0; x < nodes.getLength(); x++) {
				Node node = nodes.item(x);
				NodeList childNodes = node.getChildNodes();
				for (int y = 0; y < childNodes.getLength(); y++) {
					if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_price")) {
						winePrice5 = childNodes.item(y).getTextContent();
					}
				}
			}

			wineOrder5.setText(this.getIntent().getStringExtra("wineOrder5"));
			total5.setText(this.getIntent().getStringExtra("total5")+" bottles");
			estPrice5 = decForm.format(Double.valueOf(winePrice5) * Integer.parseInt(this.getIntent().getStringExtra("total5")));
		}

		fullName.setText(this.getIntent().getStringExtra("firstName")+" "+this.getIntent().getStringExtra("surName"));
		email.setText(this.getIntent().getStringExtra("email"));
		mobile.setText(this.getIntent().getStringExtra("mobile"));
		address.setText(this.getIntent().getStringExtra("address"));

		price.setText(decForm.format(Double.valueOf(estPrice1) + Double.valueOf(estPrice2) + Double.valueOf(estPrice3) + Double.valueOf(estPrice4) + Double.valueOf(estPrice5)));
		clubSaving = String.valueOf(decForm.format(discount * (Double.parseDouble(price.getText().toString()))));
		saving.setText("$"+clubSaving);
		Double finalTotal = Double.parseDouble(price.getText().toString()) - Double.parseDouble(clubSaving);
		totalPrice.setText("$"+String.valueOf(decForm.format(finalTotal)));
		price.setText("$"+price.getText().toString());

		// prepare data for submission

		_firstName = this.getIntent().getStringExtra("firstName").replace(" ", "+");
		_surName = this.getIntent().getStringExtra("surName").replace(" ", "+");
		_email = this.getIntent().getStringExtra("email");
		_mobile = this.getIntent().getStringExtra("mobile").replace(" ", "+");
		_address = this.getIntent().getStringExtra("address").replace(" ", "+");
		_wineClub = this.getIntent().getStringExtra("clubNumber");
		_wineOrder1 = this.getIntent().getStringExtra("selectedID1") == null ? "" : this.getIntent().getStringExtra("selectedID1");
		_wineOrder2 = this.getIntent().getStringExtra("selectedID2") == null ? "" : this.getIntent().getStringExtra("selectedID2");
		_wineOrder3 = this.getIntent().getStringExtra("selectedID3") == null ? "" : this.getIntent().getStringExtra("selectedID3");
		_wineOrder4 = this.getIntent().getStringExtra("selectedID4") == null ? "" : this.getIntent().getStringExtra("selectedID4");
		_wineOrder5 = this.getIntent().getStringExtra("selectedID5") == null ? "" : this.getIntent().getStringExtra("selectedID5");
		_wineName1 = this.getIntent().getStringExtra("wineOrder1") == null ? "" : this.getIntent().getStringExtra("wineOrder1").replace(" ", "+");
		_wineName2 = this.getIntent().getStringExtra("wineOrder2") == null ? "" : this.getIntent().getStringExtra("wineOrder2").replace(" ", "+");
		_wineName3 = this.getIntent().getStringExtra("wineOrder3") == null ? "" : this.getIntent().getStringExtra("wineOrder3").replace(" ", "+");
		_wineName4 = this.getIntent().getStringExtra("wineOrder4") == null ? "" : this.getIntent().getStringExtra("wineOrder4").replace(" ", "+");
		_wineName5 = this.getIntent().getStringExtra("wineOrder5") == null ? "" : this.getIntent().getStringExtra("wineOrder5").replace(" ", "+");
		_quantity1 = this.getIntent().getStringExtra("total1") == null ? "" : this.getIntent().getStringExtra("total1");
		_quantity2 = this.getIntent().getStringExtra("total2") == null ? "" : this.getIntent().getStringExtra("total2");
		_quantity3 = this.getIntent().getStringExtra("total3") == null ? "" : this.getIntent().getStringExtra("total3");
		_quantity4 = this.getIntent().getStringExtra("total4") == null ? "" : this.getIntent().getStringExtra("total4");
		_quantity5 = this.getIntent().getStringExtra("total5") == null ? "" : this.getIntent().getStringExtra("total5");
		_estPrice = price.getText().toString().substring(1);
		_clubSave = saving.getText().toString().substring(1);
		_total = totalPrice.getText().toString().substring(1);

		confirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								submitAPI = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_submit_order.php?"+ 
										"first_name="+_firstName+"&surname="+_surName+"&email="+_email+"&mobile="+_mobile+""+
										"&address="+_address+"&wine_club="+_wineClub+"&wine_order1="+_wineOrder1+"&wine_order2="+_wineOrder2+""+
										"&wine_order3="+_wineOrder3+"&wine_order4="+_wineOrder4+"&wine_order5="+_wineOrder5+"&"+ 
										"wine_name1="+_wineName1+"&wine_name2="+_wineName2+"&wine_name3="+_wineName3+"&wine_name4="+_wineName4+"&wine_name5="+_wineName5+"&"+ 
										"wine_qty1="+_quantity1+"&wine_qty2="+_quantity2+"&wine_qty3="+_quantity3+"&wine_qty4="+_quantity4+"&wine_qty5=&"+_quantity5+"&"+ 
										"est_price="+_estPrice+"&wine_club_saving="+_clubSave+"&total_price="+_total+"&mode=M";

								String message = null;

								// XPath initialization
								XPath xpath = XPathFactory.newInstance().newXPath();
								String expression = "//BulkOrder";
								InputSource inputSource = null;
								try {
									inputSource = new InputSource(new URL(submitAPI).openStream());
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								// XPath Query
								NodeList nodes = null;
								try {
									nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
								} catch (XPathExpressionException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

								// Loop through NodeList to store data into arrays
								for (int x = 0; x < nodes.getLength(); x++) {
									Node node = nodes.item(x);
									NodeList childNodes = node.getChildNodes();
									for (int y = 0; y < childNodes.getLength(); y++) {
										if (childNodes.item(y).getNodeName().equalsIgnoreCase("message")) {
											message = childNodes.item(y).getTextContent();
										}
									}
								}
								pd.dismiss();
								Toast.makeText(OrderConfirmationActivity.this, message, Toast.LENGTH_LONG).show();						        
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
		});


	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
