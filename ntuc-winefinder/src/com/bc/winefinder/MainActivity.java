package com.bc.winefinder;

import com.bc.winefinder.AppVersionCheck;
import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.text.Html;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {
	/** Called when the activity is first created. */

	private ListView menuList;
	private TextView header;
	public ProgressDialog pd;
	public static MainActivity group;
	private int selectedPosition;
	private View selectedView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.DEBUG);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		setContentView(R.layout.main);
		group = this;

		header = (TextView) findViewById(R.id.main_menu_header);


		//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");  
		//	    header.setTypeface(font); 
		header.setText(Html.fromHtml("A world first, it\'s the only app that pairs both <br />Western and Asian food with wine"));

		// main menu list
		menuList = (ListView) findViewById(R.id.menu_list);
		String[] menuLabels = new String[] {"PAIR FOOD WITH WINE", "PAIR WINE WITH FOOD ", "LIST ALL WINES", "TIPS & VIDEOS", "JOIN JUST WINE CLUB", "ORDER IN BULK", "SHARE"};
		int[] iconIds = new int[] {
				this.getResources().getIdentifier("pair_food_icon", "drawable", this.getPackageName()), 
				this.getResources().getIdentifier("pair_wine_icon", "drawable", this.getPackageName()), 
				this.getResources().getIdentifier("all_wines_icon", "drawable", this.getPackageName()), 
				this.getResources().getIdentifier("tips_videos_icon", "drawable", this.getPackageName()), 
				this.getResources().getIdentifier("wine_club_icon", "drawable", this.getPackageName()), 
				this.getResources().getIdentifier("order_icon", "drawable", this.getPackageName()), 
				this.getResources().getIdentifier("share_icon_new", "drawable", this.getPackageName())
		};
		CustomArrayAdapter adapter = new CustomArrayAdapter(this, menuLabels, iconIds);
		menuList.setAdapter(adapter);

		// set listener
		menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if (!isOnline()) {
					new Thread(new Runnable() {
						public void run() {
							Looper.prepare();
							Toast.makeText(MainActivity.this, "No internet connection", Toast.LENGTH_LONG).show();
							runOnUiThread(new Runnable(){
								public void run() {	
									try {
										Thread.sleep(3000);
										System.exit(0);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}
							});
							Looper.loop();
						}
					}).start();
				}
				selectedPosition = position;
				selectedView = view;


				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(MainActivity.this, "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(selectedView.getContext(), TabWidget.class);
								newIntent.putExtra("selectedTab", selectedPosition);
								startActivity(newIntent);
								pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
		});

		new Thread(new Runnable () {
			public void run() {
				startService(new Intent(MainActivity.this,AppVersionCheck.class));
				Log.d("AVCS", "Started Service");
			}
		}).start();
	}	

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}