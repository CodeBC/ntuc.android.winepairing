package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class ViewVideosActivity extends Activity {

	private TextView header;
	private TextView description;
	private String youTubeID;
	private String webCallApi;
	private String selectedID;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
		
		setContentView(R.layout.view_videos);

		selectedID = this.getIntent().getStringExtra("selectedID");
		webCallApi = "https://www.balancedfbapps.com/ntuc/modules/tipsvideos/_read_tips_article.php?content_id="+selectedID+"&mode=M";

		// header
		header = (TextView) findViewById(R.id.videos_header);  
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font);

		// description
		description = (TextView) findViewById(R.id.videos_description);  
		Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");  
		description.setTypeface(font1);

		// XPath initialization
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "//item";
		InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// XPath Query
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Loop through NodeList to store data into arrays
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			NodeList childNodes = node.getChildNodes();
			for (int y = 0; y < childNodes.getLength(); y++) {
				if (childNodes.item(y).getNodeName().equalsIgnoreCase("content_title")) {
					header.setText(childNodes.item(y).getTextContent().split(":")[1]);
				} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("content_desc")) {
					//	    			description.setText(Html.fromHtml(childNodes.item(y).getTextContent()));
					youTubeID = childNodes.item(y).getTextContent();
				}
			}
		}

		startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("http://www.youtube.com/watch?v="+youTubeID)));
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
