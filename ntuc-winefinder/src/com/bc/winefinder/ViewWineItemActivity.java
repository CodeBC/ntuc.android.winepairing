package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class ViewWineItemActivity extends Activity {
	private static String webCallApi;
	private String selectedID;
	private TextView dishList;
	private TextView wineInfo;
	private TextView header;
	private TextView header1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		setContentView(R.layout.wine_item); // to be changed later
		// get the passed ID param
		selectedID = this.getIntent().getStringExtra("selectedID");
		// initialization
		//        webCallApi = "http://www.whatsupatfairprice.com.sg/wineapp/GetWineGroupInfoXML.php?WineGroupID="+selectedID; // old
		webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_winefood.php?winetype_id="+selectedID+"&mode=M";
		header1 = (TextView) findViewById(R.id.view_wine_header1);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
		header1.setTypeface(font1);

		dishList = (TextView) findViewById(R.id.view_wine_dish_list);
		wineInfo = (TextView) findViewById(R.id.view_wine_wine_info);
		Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf"); 
		dishList.setTypeface(font2);
		wineInfo.setTypeface(font2);

		// header
		header = (TextView) findViewById(R.id.wine_item_header);  
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font);
		header.setText(this.getIntent().getStringExtra("wineTitle"));

		// XPath initialization
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "//item";
		InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// XPath Query
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Loop through NodeList to store data into arrays
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			NodeList childNodes = node.getChildNodes();
			for (int y = 0; y < childNodes.getLength(); y++) {
				if (childNodes.item(y).getNodeName().equalsIgnoreCase("winetype_desc")) {
					wineInfo.setText(Html.fromHtml(childNodes.item(y).getTextContent()));
				} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("food_names")) {
					dishList.setText(childNodes.item(y).getTextContent());
				}
			}
		}

		// Getting Dish List, using different expression
		//	    String dishListText = "";
		//	    expression = "//FoodItem";
		//	    inputSource = null;
		//		try {
		//			inputSource = new InputSource(new URL(webCallApi).openStream());
		//		} catch (MalformedURLException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//		
		//		// XPath Query
		//		nodes = null;
		//	    try {
		//			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		//		} catch (XPathExpressionException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		//	    
		//	    // Loop through NodeList to store data into arrays
		//	    for (int x = 0; x < nodes.getLength(); x++) {
		//	    	Node node = nodes.item(x);
		//	    	NodeList childNodes = node.getChildNodes();
		//	    	if (x != 0) {
		//	    		dishListText += ", ";
		//	    	}
		//	    	for (int y = 0; y < childNodes.getLength(); y++) {
		//	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("FoodName")) {
		//	    			dishListText += childNodes.item(y).getTextContent();
		//	    		}
		//	    	}
		//	    }

		// set text for wineList View
		//	    dishList.setText(dishListText);
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
