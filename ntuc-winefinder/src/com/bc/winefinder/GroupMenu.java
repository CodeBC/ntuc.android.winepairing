package com.bc.winefinder;

public class GroupMenu {
	private String id;
	private String title;
	private String icon;
	
	GroupMenu(String id, String title, String icon) {
		this.id = id;
		this.title = title;
		this.icon = icon;
	}
	
	GroupMenu(String id, String title) {
		this.id = id;
		this.title = title;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getIcon() {
		return this.icon;
	}
	
}
