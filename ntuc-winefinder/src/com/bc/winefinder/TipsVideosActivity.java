package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class TipsVideosActivity extends Activity {

	private Button tips;
	private Button videos;
	private TextView header;
	private TextView info;
	private ListView tipsList;
	private SimpleArrayAdapter adapter;
	private ProgressDialog pd;
	private String webCallApi;
	private ArrayList<String> tipsArr;
	private ArrayList<GroupMenu> tipItems;
	private int selectedPosition;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
	    setContentView(R.layout.tips_videos);
	    
	    webCallApi = "https://www.balancedfbapps.com/ntuc/modules/tipsvideos/_read_tips.php?mode=M";
	    tipsArr = new ArrayList<String>();
	    tipItems = new ArrayList<GroupMenu>();
	    
	    tips = (Button) findViewById(R.id.tips_btn);
	    videos = (Button) findViewById(R.id.videos_btn);
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    tips.setTypeface(font2);
	    videos.setTypeface(font2);
	    
	    videos.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								// Trigger the next intent and pass the Group Dish Object ID
					    		Intent newIntent = new Intent(getParent(), VideosTipsActivity.class);
					    		View v = FourthTabGroup.group.getLocalActivityManager()
					    				.startActivity("VideosTipsActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		FourthTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
	    });
	    
	    header = (TextView) findViewById(R.id.tips_videos_header);
	    info = (TextView) findViewById(R.id.tips_videos_info);
	    
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font); 
	    header.setText("TIPS & VIDEOS");
	    
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");  
	    info.setText("There is a special pleasure that comes from knowing your wines. And know them will, when you go through the comprehensive list of tips and videos we've put together specially for you.");
	    info.setTypeface(font1);
	    
	    // XPath initialization
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    String expression = "//item";
	    InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		NodeList nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Loop through NodeList to store data into arrays
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	String id = null;
	    	String title = null;
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("content_id")) {
	    			id = childNodes.item(y).getTextContent();
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("content_title")) {
	    			title = childNodes.item(y).getTextContent().toUpperCase();
	    			tipsArr.add(title);
	    		}
	    	}
	    	tipItems.add(new GroupMenu(id, title));
	    }
	    
	    tipsList = (ListView) findViewById(R.id.tips_list);
	    adapter = new SimpleArrayAdapter(this, R.layout.row, tipsArr);
        tipsList.setAdapter(adapter);
        
        FourthTabGroup.group.changeTabWidgetOnBackButtonClick();
        
        tipsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub
				
				selectedPosition = position;
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								// Trigger the next intent and pass the Group Dish Object ID
					    		Intent newIntent = new Intent(getParent(), ViewTipsActivity.class);
					    		newIntent.putExtra("selectedID", getItemIdByTitle(adapter.getItem(selectedPosition)));
					    		// Create the view using FirstTabGroup's LocalActivityManager
					    		View v = FourthTabGroup.group.getLocalActivityManager()
					    				.startActivity("ViewTipsActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		FourthTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
		});   
        
    }
	
	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : tipItems) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}
		
		return null;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
