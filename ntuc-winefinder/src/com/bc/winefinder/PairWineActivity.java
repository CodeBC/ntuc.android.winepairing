package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PairWineActivity extends Activity {

	private static String webCallApi;
	private ArrayList<String> groupWineTitle; 
	private ArrayList<GroupMenu> groupWineArr; 
	private ListView wineTypeList;
	private TextView header;
	private SimpleArrayAdapter adapter;
	private ProgressDialog pd;
	private EditText searchText;
	private Button addWine;
	private int selectedPosition;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
        setContentView(R.layout.pair_wine);
        // initialization
        webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine_type.php?mode=M";
	    groupWineTitle = new ArrayList<String>();
	    groupWineArr = new ArrayList<GroupMenu>();
	    searchText = (EditText) findViewById(R.id.wine_type_search);
	    
	    // header
	    header = (TextView) findViewById(R.id.pair_wine_header);  
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font); 
	    
	    addWine = (Button) findViewById(R.id.add_wine_btn_new);
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    addWine.setTypeface(font2); 
	    addWine.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), AddWineFormActivity.class);
								// Create the view using FirstTabGroup's LocalActivityManager
						        View v = SecondTabGroup.group.getLocalActivityManager()
						        .startActivity("AddWineFormActivity", newIntent
						        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						        .getDecorView();

						        // Again, replace the view
						        SecondTabGroup.group.replaceView(v);
						        // Clear searchText focus
						        searchText.clearFocus();
						        searchText.setText("");
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
				
			}
	    });
	    
	    // add listener to check if searchText box has focus, if not then hide the keyboard
	    searchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
	        public void onFocusChange(View v, boolean hasFocus) {
	            if (!hasFocus) {
	            	InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
	                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

	            }
	        }
	    });
	    
	    // XPath initialization
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    String expression = "//item";
	    InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		NodeList nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Loop through NodeList to store data into arrays
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	String id = null;
	    	String title = null;
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("winetype_id")) {
	    			id = childNodes.item(y).getTextContent();
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("winetype_name")) {
	    			title = childNodes.item(y).getTextContent().toUpperCase();
	    			groupWineTitle.add(title);
	    		} 
	    	}
	    	groupWineArr.add(new GroupMenu(id, title));	
	    }
	    
	    // Create ListView using the array of titles
	    wineTypeList = (ListView) findViewById(R.id.pair_wine_list);
	    adapter = new SimpleArrayAdapter(this, R.layout.row, groupWineTitle);
        wineTypeList.setAdapter(adapter);
        
        SecondTabGroup.group.changeTabWidgetOnBackButtonClick();
        
        // Implement search function for searcText
        searchText.addTextChangedListener(filterTextWatcher);
        
        // set listener
        wineTypeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub
				// Trigger the next intent and pass the Group Dish Object ID
//				Intent newIntent = new Intent(view.getContext(), ViewWineListActivity.class); //old
		
				selectedPosition = position;
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), ViewWineItemActivity.class);
								newIntent.putExtra("selectedID", getItemIdByTitle(adapter.getItem(selectedPosition))); 
								newIntent.putExtra("wineTitle", adapter.getItem(selectedPosition));
								// Create the view using SecondTabGroup's LocalActivityManager

								Window w = SecondTabGroup.group.getLocalActivityManager()
								.startActivity("ViewWineListActivity", newIntent
										.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

								// Again, replace the view
								SecondTabGroup.group.replaceView(w.getDecorView());
								// Clear searchText focus
								searchText.clearFocus();
								searchText.setText("");
								pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
				
			}
		});
        
    }
	
	// For Search Function
	private TextWatcher filterTextWatcher = new TextWatcher() {

	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	    }

	    public void onTextChanged(CharSequence s, int start, int before, int count) {
	        adapter.getFilter().filter(s); // Filter from my adapter
	        adapter.notifyDataSetChanged(); // Update my view
	    }

		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub
			
		}

	};
	
	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : groupWineArr) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}
		
		return null;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
