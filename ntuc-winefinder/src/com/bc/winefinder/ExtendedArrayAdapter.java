package com.bc.winefinder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

public class ExtendedArrayAdapter extends ArrayAdapter<String> implements Filterable {
	
	private final Context context;
	private ArrayList<String> texts;
	private ArrayList<Drawable> icons;
	private ArrayList<String> originalTexts;
	private Filter mFilter;
	
	public ExtendedArrayAdapter(Activity context, ArrayList<String> texts, ArrayList<Drawable> icons) {
		super(context, R.layout.custom_row, texts);
		this.texts = texts;
		this.context = context;
		this.icons = icons;
		this.originalTexts = texts;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.custom_row, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.menu_label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.list_icon);
		
		Typeface font1 = Typeface.createFromAsset(this.getContext().getApplicationContext().getAssets(), "fonts/DIN Medium.ttf");  
	    textView.setTypeface(font1); 
		textView.setText(texts.get(position));
		imageView.setImageDrawable(icons.get(position));
		
		return rowView;
	}
	
	@Override
	public String getItem(int position) {
		return texts.get(position);
	}
	
	public void add(String object) {
		originalTexts.add(object);
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return texts.size();
	}
	
	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new CustomFilter();
		}
		
		return mFilter;
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}
	
	private class CustomFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			FilterResults results = new FilterResults();
			if (constraint == null || constraint.length() == 0) {
				ArrayList<String> list = new ArrayList<String>(originalTexts);
				results.values = list;
				results.count = list.size();
			} else {
				ArrayList<String> newValues = new ArrayList<String>();
				for (int x = 0; x < originalTexts.size(); x++) {
					String item = originalTexts.get(x);
					if (item.contains(constraint.toString().toUpperCase())) {
						newValues.add(item);
					}
				}
				results.values = newValues;;
				results.count = newValues.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// TODO Auto-generated method stub
			texts = (ArrayList<String>) results.values;
			notifyDataSetChanged();
		}
		
	}
}
