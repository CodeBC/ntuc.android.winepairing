package com.bc.winefinder;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SharingActivity extends Activity {

	private TextView header;
	private Button facebookSharing;
	private Button twitterSharing;
	private ProgressDialog pd;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
        setContentView(R.layout.sharing);
        
        // header
	    header = (TextView) findViewById(R.id.sharing_header);  
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font);
	    
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");
	    facebookSharing = (Button) findViewById(R.id.facebook_sharing_btn);
	    twitterSharing = (Button) findViewById(R.id.twitter_sharing_btn);
	    facebookSharing.setTypeface(font2);
	    twitterSharing.setTypeface(font2);
        
	    facebookSharing.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), FacebookSharingActivity.class);
					    		View v = SeventhTabGroup.group.getLocalActivityManager()
					    				.startActivity("FacebookSharingActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		SeventhTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();

			}
	    });
	    
	    twitterSharing.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), TwitterSharingActivity.class);
					    		View v = SeventhTabGroup.group.getLocalActivityManager()
					    				.startActivity("TwitterSharingActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		SeventhTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
				
			}
	    });
	    
        SeventhTabGroup.group.changeTabWidgetOnBackButtonClick();
    }

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
