package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ViewDishItemActivity extends Activity {
	
	private static String webCallApi;
	private String selectedID;
	private String selectedWineID;
	private TextView wineList;
	private TextView wineInfo;
	private TextView foodInfo;
	private TextView header;
	private TextView header1;
	private Button fairpriceRecommneds;
	private ProgressDialog pd;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
        setContentView(R.layout.dish_item); // to be changed later
        // get the passed ID param
        selectedID = this.getIntent().getStringExtra("selectedID");
        // initialization
        webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_food.php?food_id="+selectedID+"&mode=M";
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");
        
        wineList = (TextView) findViewById(R.id.view_dish_wine_list);
	    wineInfo = (TextView) findViewById(R.id.view_dish_wine_info);
	    foodInfo = (TextView) findViewById(R.id.view_dish_food_info);
	    wineList.setTypeface(font1);
	    wineInfo.setTypeface(font1);
	    foodInfo.setTypeface(font1);
	    
	    fairpriceRecommneds = (Button) findViewById(R.id.fairprice_recommends_btn);
	    header1 = (TextView) findViewById(R.id.view_dish_header1);
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    header1.setTypeface(font2); 
	    fairpriceRecommneds.setTypeface(font2);
	    
	    // header
	    header = (TextView) findViewById(R.id.dish_item_header);  
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font);
	    header.setText(this.getIntent().getStringExtra("dishTitle"));
        
        // XPath initialization
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    String expression = "//item";
	    InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		NodeList nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Loop through NodeList to store data into arrays
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("food_desc")) {
	    			foodInfo.setText(Html.fromHtml(childNodes.item(y).getTextContent()));
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("DishWineInfo")) {
	    			wineInfo.setText(Html.fromHtml(childNodes.item(y).getTextContent()));
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_id")) {
	    			selectedWineID = childNodes.item(y).getTextContent();
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("winetype_name")) {
	    			wineList.setText(childNodes.item(y).getTextContent()); 
	    		}
	    	}
	    }
	    
	    fairpriceRecommneds.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), ViewRecommendActivity.class);
					    		newIntent.putExtra("selectedID", selectedWineID);
					    		View v = FirstTabGroup.group.getLocalActivityManager()
					    				.startActivity("ViewRecommendActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		FirstTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();	
			}    	
	    });
	}
	
	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
