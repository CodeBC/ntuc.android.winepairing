package com.bc.winefinder;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class OrderActivity extends Activity {

	private TextView orderInfo;
	private Button placeOrder;
	private Button becomeMember;
	private TextView header;
//	private Button testMap;
	private ProgressDialog pd;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
	    setContentView(R.layout.order_page1);
	    
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");
	    orderInfo = (TextView) findViewById(R.id.order_page_info);
	    orderInfo.setTypeface(font1);
	   
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");
	    placeOrder = (Button) findViewById(R.id.order_btn);
	    becomeMember = (Button) findViewById(R.id.become_member_btn);
	    placeOrder.setTypeface(font2);
	    becomeMember.setTypeface(font2);
//	    testMap = (Button) findViewById(R.id.test_map);
	    
//	    testMap.setOnClickListener(new OnClickListener() {
//			public void onClick(View view) {
//				// TODO Auto-generated method stub
//				Intent newIntent = new Intent(view.getContext(), MapWebViewActivity.class);
//				// Create the view using SixthTabGroup's LocalActivityManager
//		        View v = SixthTabGroup.group.getLocalActivityManager()
//		        .startActivity("MapWebViewActivity", newIntent
//		        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
//		        .getDecorView();
//
//		        // Again, replace the view
//		        SixthTabGroup.group.replaceView(v);
//			}
//	    });
	    
	    header = (TextView) findViewById(R.id.order_wine_header);
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font); 
	    
	    String orderInfoHTML = "&#8226; FairPrice accepts bulk orders<br />" +
	    						"&#8226; Minimum order of 5 cases and above<br />" +
	    						"&#8226; Delivery only available in mainland Singapore<br />" +
	    						"&#8226; Cash On Delivery<br />" +
	    						"&#8226; Please allow a minimum of 10 working days for delivery<br />" +
	    						"&#8226; Order placement by case only (individual bottle selection not available)<br />" +
	    						"&#8226; Orders placed will be confirmed by email within 5 working days<br />" +
	    						"&#8226; Payment must be made 3 days before delivery<br />" +
	    						"&#8226; No LinkPoint/Rebates will be given for order in bulk<br />";
	    orderInfo.setText(Html.fromHtml(orderInfoHTML));
	    
	    becomeMember.setText(Html.fromHtml("BECOME JUST WINE CLUB MEMBER <font size='8px'>(SAVE AN EXTRA 8%)</font>"));
	    becomeMember.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TabWidget.group.getTabHost().setCurrentTab(4);
			}
	    });
	    
	    placeOrder.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), OrderFormActivity.class);
								// Create the view using SixthTabGroup's LocalActivityManager
						        View v = SixthTabGroup.group.getLocalActivityManager()
						        .startActivity("OrderFormActivity", newIntent
						        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						        .getDecorView();

						        // Again, replace the view
						        SixthTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}	
	    });
	    
	    SixthTabGroup.group.changeTabWidgetOnBackButtonClick();
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
