package com.bc.winefinder;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;

import com.bc.winefinder.AppCheckDialog;
import com.bc.winefinder.util.HTTPPost;
import com.bc.winefinder.util.NetworkListener;

public class AppVersionCheck extends Service{
	private Looper mServiceLooper;
	private static ServiceHandler mServiceHandler;
	private String TAG = "AVCS";

	String url = "https://balancedfbapps.com/ntuc/modules/client/_ext_check_app_version.php";

	String status,link;
	float version;

	NotificationManager nm;

	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}
		@Override
		public void handleMessage(Message msg) {

			if (NetworkListener.isOnline(getApplicationContext())){
				HTTPPost post = new HTTPPost();

				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
				nameValuePairs.add(new BasicNameValuePair("client_platform", "A"));
				String result = post.postHttpData(url, nameValuePairs);
				Log.i(TAG,"HTTP Result : "+result);
				if (result != null) parseJSON(result);
				Log.d("AVCS", result);
			}
			
			

			stopSelf(msg.arg1);
		}
	}

	@Override
	public void onCreate() {
		// Start up the thread running the service.  Note that we create a
		// separate thread because the service normally runs in the process's
		// main thread, which we don't want to block.  We also make it
		// background priority so CPU-intensive work will not disrupt our UI.
		HandlerThread thread = new HandlerThread("ServiceStartArguments",Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		Log.d("AVCS", "On Create Service");
		mServiceLooper = thread.getLooper();
		mServiceHandler = new ServiceHandler(mServiceLooper);

		nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		//Toast Notification
		//Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
		Log.i(TAG, "Service : App VersionCheck Start");
		// For each start request, send a message to start a job and deliver the
		// start ID so we know which request we're stopping when we finish the job
		Message msg = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);

		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		Log.d("AVCS", "On Bind Service");
		return null;
	}

	@Override
	public void onDestroy() {
		//Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
		Log.i(TAG, "Service : App VersionCheck Stop");
	}

	private void parseJSON(String json){
		Log.d("AVCS", "On Parse Json");
		try {
			JSONObject obj = new JSONObject(json);

			//Status
			if (obj.has("status")){
				status = obj.getString("status");
			}else{
				status = "";
			}

			//Version
			if (obj.has("version")){
				version = Float.parseFloat(obj.getString("version"));
			}else{
				version = 0;
			}

			//Link
			if (obj.has("link")){
				link = obj.getString("link");
			}else{
				link = "";
			}

			Log.i(TAG,"Status : " + status);
			Log.i(TAG,"Version : " + version);
			Log.i(TAG,"Message : " + link);

			PackageManager pm = getApplicationContext().getPackageManager();
			PackageInfo pi = pm.getPackageInfo("com.bc.winefinder", 0);
			float versioncode = Float.parseFloat(pi.versionName);
			Log.i(TAG,"Version Code: " + versioncode);
			Log.i(TAG,"Version Code From Server: " + version);
			//if (versioncode == version)showNotification("Version " + version + " is available", link);
			if (versioncode < version){
				showNotification("Version " + version + " is available", link);
				Intent i = new Intent(getApplicationContext(), AppCheckDialog.class);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.putExtra("link", link);
				startActivity(i);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showNotification(String content,String link) {
		// Set the icon, scrolling text and timestamp
		Notification notification = new Notification(R.drawable.winefinder_icon, content, System.currentTimeMillis());
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
		notificationIntent.setData(Uri.parse(link));

		// The PendingIntent to launch our activity if the user selects this notification
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		// Set the info for the views that show in the notification panel.
		notification.setLatestEventInfo(this, "NTUC WineFinder", content, contentIntent);

		// Send the notification.
		// We use a layout id because it is a unique number.  We use it later to cancel.
		nm.notify(10, notification);
	}

	private void showDialog(String content,final String link){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		// set title
		alertDialogBuilder.setTitle("Your Title");

		// set dialog message
		alertDialogBuilder
		.setMessage("Click yes to exit!")
		.setCancelable(false)
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(link));
				startActivity(i);
			}
		})
		.setNegativeButton("No",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

}