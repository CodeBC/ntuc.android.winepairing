package com.bc.winefinder.util;

public interface Constants {
	public static final int SPLASH_WAIT_MILLISEC = 3000;

	public static final String DEFAUL_SHARE_DATA = "DEFAUL_SHARE_DATA";

	public static final String FORMULA_SHARE_DATA = "FORMULAT_SHARE_DATA";

	public static final String HISTORY_SHARE_DATA = "HISTORY_SHARE_DATA";

	//public static final String BASE_URL = "http://192.168.2.100/sg1/";
	public static final String BASE_URL = "http://test.balancedconsultancy.com.sg/tsg1/";

	public static final String FACEBOOK_APP_ID = "342287095849116";
	
	public static final String inmobi_id = "30cdfe13-1a97-4dfc-8e5e-a44a01fb1245";

	public static final String TWITTER_CONSUMER_KEY = "4FYtR8PXID6vZjsK41zpA";
	public static final String TWITTER_CONSUMER_SECRET = "epZHeok28jNHoAN3Zx3HuVJytCuG8p6icKzch37kQ";

	public static final String FACEBOOK_SHARE_MESSAGE = "Test Share Message";
	public static final String FACEBOOK_SHARE_LINK = "http://singaporeg1.sg/";
	public static final String FACEBOOK_SHARE_LINK_NAME = "Singapore G1";
	public static final String FACEBOOK_SHARE_LINK_DESCRIPTION = "Singapore G1";
	public static final String FACEBOOK_SHARE_PICTURE = "http://singaporeg1.sg/web/images/logo_singapore_g1.png";
	public static final String FACEBOOK_SHARE_ACTION_NAME = "Sharing G1 result";
	public static final String FACEBOOK_SHARE_ACTION_LINK = "http://singaporeg1.sg/";
	public static final String FACEBOOK_SHARE_IMAGE_CAPTION = "G1 Icon";

	public static final String TWITTER_SHARE_MESSAGE = "Test Share Message";

	public static final String REQUEST_URL = "http://api.twitter.com/oauth/request_token";
	public static final String ACCESS_URL = "http://api.twitter.com/oauth/access_token";
	public static final String AUTHORIZE_URL = "http://api.twitter.com/oauth/authorize";

	public static final String	OAUTH_CALLBACK_SCHEME	= "x-oauthflow-twitter";
	public static final String	OAUTH_CALLBACK_HOST		= "callback";
	public static final String	OAUTH_CALLBACK_URL		= OAUTH_CALLBACK_SCHEME + "://" + OAUTH_CALLBACK_HOST;

	public static final class Extra {
		public static final String POST_MESSAGE = "com.bc.livegreen.extra.POST_MESSAGE";
		public static final String POST_LINK = "com.bc.livegreen.extra.POST_LINK";
		public static final String POST_PHOTO = "com.bc.livegreen.extra.POST_PHOTO";
		public static final String POST_PHOTO_DATE = "com.bc.livegreen.extra.POST_PHOTO_DATE";
		public static final String POST_LINK_NAME = "com.bc.livegreen.extra.POST_LINK_NAME";
		public static final String POST_LINK_DESCRIPTION = "com.bc.livegreen.extra.POST_LINK_DESCRIPTION";
		public static final String POST_PICTURE = "com.bc.livegreen.extra.POST_PICTURE";
	}

}
