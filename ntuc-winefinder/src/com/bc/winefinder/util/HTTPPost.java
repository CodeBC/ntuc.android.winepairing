package com.bc.winefinder.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import org.json.JSONObject;
import android.util.Log;

public class HTTPPost {
	
	public static final String ERROR_CODE_TIME_OUT = "408";
	public static final String ERROR_CODE_UNKNOW_HOST = "404";
	private String TAG = "HTTP";

	public HttpResponse sendJSON(JSONObject jo,String url) throws Exception{
		Log.i(TAG,"HTTP Post : " + jo.toString());
		DefaultHttpClient httpClient = new DefaultHttpClient();
	    HttpPost postMethod = new HttpPost(url);
	    postMethod.setHeader( "Content-Type", "application/json" );
	    postMethod.setEntity(new ByteArrayEntity(jo.toString().getBytes("UTF8")));
	    HttpResponse response = httpClient.execute(postMethod);
	    Log.i(TAG,"HTTP Response : " + response.getStatusLine());
	    Log.i(TAG,"HTTP Response Code: " + response.getStatusLine().getStatusCode());
	    Header[] headers = response.getAllHeaders();
	    for (int i=0; i < headers.length; i++) {
	    	Header h = headers[i];
	    	Log.i(TAG,h.getName() + ":" + h.getValue());
	    }
	    
	    return response;
	}
	
	public String postHttpData(String url,List<NameValuePair> nameValuePairs ){
		Log.i(TAG,"postUrlData ======================>>> " + url);
		String result = null;
		int timeOutMS = 1000*10;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
		    HttpPost postMethod = new HttpPost(url);
		    //postMethod.setHeader( "Content-Type", "application/json" );
		    
		    //Try 2
		    postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		    Log.i(TAG,"HTTP Entiry : " + convertStreamToString(postMethod.getEntity().getContent()));
		    HttpResponse response = httpClient.execute(postMethod);
		    Log.i(TAG,"HTTP Response : " + response.getStatusLine());
		    Log.i(TAG,"HTTP Response Code: " + response.getStatusLine().getStatusCode());
		    if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				response.getEntity().writeTo(os);
				result = os.toString();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
	
	public String getHttpData(String url) {
		Log.i(TAG,"getUrlData ======================>>> " + url);
		String result = null;
		int timeOutMS = 1000*100;
		HttpResponse response = null;
		try {
			HttpParams my_httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(my_httpParams, timeOutMS);
			HttpConnectionParams.setSoTimeout(my_httpParams, timeOutMS);
			// HttpClient httpClient = new DefaultHttpClient(my_httpParams); //

			DefaultHttpClient client = new DefaultHttpClient(my_httpParams);
			URI uri = new URI(url);
			HttpGet httpGetRequest = new HttpGet(uri);
			response = client.execute(httpGetRequest);
			Log.i(TAG,"HTTP Response : " + response.getStatusLine());
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				response.getEntity().writeTo(os);
				result = os.toString();
			}

		}
		catch (Exception e){
			e.printStackTrace();
			result = response.getStatusLine().getStatusCode()+"";
		}
		
		return result;
	}
	
	public String toJSONString(String result){
		if(result.startsWith("("))
			return result.substring(1);
		if(!result.startsWith("{")){
			int index = result.indexOf("{");
			return result.substring(index);
		}
		
		else
			return result;
	}
	
	private String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;

	    while ((line = reader.readLine()) != null) {
	        sb.append(line);
	    }

	    is.close();

	    return sb.toString();
	}
}
