package com.bc.winefinder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewRecommendActivity extends Activity {
	
	private TextView header;
	private static String webCallApi;
	private String selectedID;
	private TextView title;
	private TextView description;
	private TextView membership;
	private ImageView wineImage;
	private TextView price;
	private ImageView dollarSign;
	private String rating;
	private TextView rateThis;
	private Button locateBtn;
	private ImageView locationMarker;
	private int[] starSequence;
	private int selectedRating;
	private ProgressDialog pd;
	private String ratingApi;
	private InputSource inputSource;
	private TextView excludeInfo;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
		
        setContentView(R.layout.fairprice_recommend);
        
        selectedID = this.getIntent().getStringExtra("selectedID");
        webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id="+selectedID;
        locateBtn = (Button) findViewById(R.id.locate_hotspot);
        locationMarker = (ImageView) findViewById(R.id.location_marker);
        
        locateBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), MapWebViewActivity.class);
					    		// Create the view using FirstTabGroup's LocalActivityManager
					    		View v = FirstTabGroup.group.getLocalActivityManager()
					    				.startActivity("MapWebViewActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		FirstTabGroup.group.replaceView(v);
					    		pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();

			}
        });
        
        // header
	    header = (TextView) findViewById(R.id.wine_details_header);  
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font); 
	    
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");
	    title = (TextView) findViewById(R.id.wine_item_title);
	    title.setTypeface(font1);
	    
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");
	    description = (TextView) findViewById(R.id.wine_item_description);
	    description.setTypeface(font2);
	    
	    membership = (TextView) findViewById(R.id.wine_item_membership);
	    excludeInfo = (TextView) findViewById(R.id.wine_item_exclude);
	    membership.setTypeface(font2);
	    excludeInfo.setTypeface(font2);
	    
	    wineImage = (ImageView) findViewById(R.id.wine_item_image);
	    dollarSign = (ImageView) findViewById(R.id.dollar_sign);
	    
	    price = (TextView) findViewById(R.id.wine_item_price);
	    price.setTypeface(font1);
	    rateThis = (TextView) findViewById(R.id.rate_this);
	    rateThis.setTypeface(font1); 
	    
	    membership.setText(Html.fromHtml("<font color='red'>Join Just Wine Club</font><font color='white'> and save 8% on wine*</font>"), TextView.BufferType.SPANNABLE);
	    membership.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TabWidget.group.getTabHost().setCurrentTab(4);
			}
        });
	    
	    excludeInfo.setText(Html.fromHtml("<p>*Exclude Japanese and Chinese wines, liquor, beer and other alcoholic drinks.</p>"));
	    
	    // XPath initialization
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    String expression = "//item";
	    inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		NodeList nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Loop through NodeList to store data into arrays
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_name")) {
	    			title.setText(childNodes.item(y).getTextContent().toUpperCase());
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_price")) {
	    			price.setText(childNodes.item(y).getTextContent());
	    			price.setTextSize(26);
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_rating")) {
	    			rating = childNodes.item(y).getTextContent();
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_desc")) {
	    			description.setText(Html.fromHtml(childNodes.item(y).getTextContent()));
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_image")) {
	    			// get image from url
	    			URL url = null;
	    			HttpURLConnection connection = null;
	    			InputStream is = null;
	    			try {
	    				url = new URL("https://www.balancedfbapps.com/ntuc/images/"+childNodes.item(y).getTextContent());
	    			} catch (MalformedURLException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}
	    			try {
	    				connection = (HttpURLConnection) url.openConnection();
	    			} catch (IOException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}		
	    			try {
	    				is = connection.getInputStream();
	    			} catch (IOException e) {
	    				// TODO Auto-generated catch block
	    				e.printStackTrace();
	    			}

	    			Bitmap img = BitmapFactory.decodeStream(is);
	    			if (Integer.valueOf(android.os.Build.VERSION.SDK) == 10) {
	    				if (img != null) {
	    					BitmapDrawable resizedImg = new BitmapDrawable(Bitmap.createScaledBitmap(img, 150, 430, true));
	    					wineImage.setImageDrawable(resizedImg);
	    				} else {
	    					wineImage.setImageBitmap(img);
	    				}
	    			} else {
	    				wineImage.setImageBitmap(img);
	    			}
	    		}
	    	}
	    }
	    
	    double ratingCount = Double.parseDouble(rating);
	    starSequence = new int[] {R.id.rating_first, R.id.rating_second, R.id.rating_third, R.id.rating_fourth, R.id.rating_fifth};
	    for (int x = 0; x < 5; x++) {
	    	ImageView tmp = (ImageView) findViewById(starSequence[x]);
	    	if (ratingCount - x >= 1.0) {
	    		tmp.setImageResource(R.drawable.rating_full_new);
	    	} else {
	    		double tmpCount = ratingCount - x;
	    		if (tmpCount > 0 && tmpCount <= 0.1) {
	    			tmp.setImageResource(R.drawable.rating_zero_one);
	    		} else if (tmpCount > 0.1 && tmpCount <= 0.2) {
	    			tmp.setImageResource(R.drawable.rating_zero_two);
	    		} else if (tmpCount > 0.2 && tmpCount <= 0.3) {
	    			tmp.setImageResource(R.drawable.rating_zero_three);
	    		} else if (tmpCount > 0.3 && tmpCount <= 0.4) {
	    			tmp.setImageResource(R.drawable.rating_zero_four);
	    		} else if (tmpCount > 0.4 && tmpCount <= 0.5) {
	    			tmp.setImageResource(R.drawable.rating_zero_five);
	    		} else if (tmpCount > 0.5 && tmpCount <= 0.6) {
	    			tmp.setImageResource(R.drawable.rating_zero_six);
	    		} else if (tmpCount > 0.6 && tmpCount <= 0.7) {
	    			tmp.setImageResource(R.drawable.rating_zero_seven);
	    		} else if (tmpCount > 0.7 && tmpCount <= 0.8) {
	    			tmp.setImageResource(R.drawable.rating_zero_eight);
	    		} else if (tmpCount > 0.8 && tmpCount <= 0.9) {
	    			tmp.setImageResource(R.drawable.rating_zero_nine);
	    		} else {
	    			tmp.setImageResource(R.drawable.rating_empty_new);
	    		}
	    	}
	    }
	    
	    // add listeners for rating images
	    for (int x = 0; x < 5; x++) {
	    	ImageView tmp = (ImageView) findViewById(starSequence[x]);
	    	tmp.setTag(String.valueOf(x));
	    	tmp.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					selectedRating = Integer.parseInt((String) v.getTag()) + 1;
					updateRating(Integer.parseInt((String) v.getTag()));
				}
	    	});
	    }
	    
	    // add listener for rateThis
	    rateThis.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ratingApi = "https://www.balancedfbapps.com/ntuc/modules/rating/_update_rating.php?wine_id="+selectedID+"&rating="+selectedRating+"";
				inputSource = null;
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								try {
									inputSource = new InputSource(new URL(ratingApi).openStream());
									pd.dismiss();
									Toast.makeText(ViewRecommendActivity.this, "Rating submitted. Thank you for rating", Toast.LENGTH_LONG).show();
								} catch (MalformedURLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
						        
							}
						}); 
						Looper.loop();
					}
				}).start();

			}
	    });
	}
	
	public void updateRating(int seq) {
		clearRating();
		for (int x = 0; x <= seq; x++) {
			ImageView tmp = (ImageView) findViewById(starSequence[x]);
			tmp.setImageResource(R.drawable.rating_full_new);
		}
	}
	
	public void clearRating() {
		for (int x = 0; x < starSequence.length; x++) {
			ImageView tmp = (ImageView) findViewById(starSequence[x]);
			tmp.setImageResource(R.drawable.rating_empty_new);
		}
	}
	
	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
