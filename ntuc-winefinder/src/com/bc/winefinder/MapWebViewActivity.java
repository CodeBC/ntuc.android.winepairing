package com.bc.winefinder;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class MapWebViewActivity extends Activity implements LocationListener {

	//	private static final String WEBVIEW_FILE_PATH = "file:///android_asset/map.html";
	private static final String WEBVIEW_FILE_PATH = "https://www.balancedfbapps.com/ntuc/map/map.html";
	private WebView webView;
	private Location mostRecentLocation;
	private TextView header;

	@Override
	/** Called when the activity is first created. */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		setContentView(R.layout.map_webview);
		getLocation();
		setupWebView();
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		header = (TextView) findViewById(R.id.map_header);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font); 
	}
	/** Sets up the WebView object and loads the URL of the page **/
	private void setupWebView(){
		final String centerURL = "javascript:centerAt(" +
				mostRecentLocation.getLatitude() + "," +
				mostRecentLocation.getLongitude()+ ")";
		// putting markers
		// NTUC Xtra
		//		String blue = "[\"1.369495,103.848434\"," + 
		//				"\"1.375667,103.87945\"," +
		//				"\"1.339408,103.705372\"," +
		//				"\"1.350745,103.872802\"]";
		//		
		//		String blueBranches = "[\"53 Ang Mo Kio Ave 3 <br />Ang Mo Kio Hub #B2-26 S(569933)\"," + 
		//				"\"No.1 Hougang St 91 <br />#02-01 Hougang Point S(538692)\"," + 
		//				"\"63 Jurong West Central 3 <br />#03-01 Jurong Point S(648331)\"," + 
		//				"\"23 Serangoon Central <br />#03-42 Nex Mall S(556083)\"]";	
		//		
		//		// NTUC Finest
		//		String orange = "[\"1.338808,103.778239\"," + 
		//				"\"1.352361,103.943785\"," +
		//				"\"1.301943,103.907595\"," +
		//				"\"1.35465,103.830945\"," +
		//				"\"1.300213,103.837286\"," +
		//				"\"1.350285,103.848278\"," + 
		//				"\"1.365362,103.865232\"," + 
		//				"\"1.313989,103.765536\"," + 
		//				"\"1.305772,103.833063\"]";
		//		
		//		String orangeBranches = "[\"#B1-01 & #B2-01 Bukit Timah Plaza <br />No.1 Jln Anak Bukit S(588996)\"," + 
		//				"\"2 Tampines Central 5 <br />#B1-07/08 & 17 Century Square S(529509)\"," + 
		//				"\"No. 6 Marine Parade Central <br />#01-670 S(449411)\"," + 
		//				"\"301 Upper Thomson Rd <br />#03-37/38 Thomson Plaza S(574408)\"," + 
		//				"\"111 Somerset Road #01-04/04A/05 to 09<br />Triple One Somerset S(238164)\"," + 
		//				"\"9 Bishan Place #B1-01 <br />Junction 8 Shopping Centre S(579837)\"," + 
		//				"\"1 Maju Ave #B1-11 to #B1-K09 <br />Serangoon Garden Village S(556679)\"," + 
		//				"\"3155 Commonwealth Avenue West <br />#B1-12/13/14 The Clementi Mall S(129588)\"," + 
		//				"\"6 Scotts Road #B1-03 to 07 and #B1-10 <br />Scotts Square S(228209)\"]";
		//		
		//		// NTUC Super Mart
		//		String red = "[\"1.371409,103.847083\"," + 
		//				"\"1.283119,103.817643\"," +
		//				"\"1.325923,103.931635\"," +
		//				"\"1.380576,103.764093\"," +
		//				"\"1.348888,103.849692\"," +
		//				"\"1.449543,103.818726\"," + 
		//				"\"1.311383,103.856627\"," + 
		//				"\"1.342706,103.953034\"," + 
		//				"\"1.372085,103.893659\"," +
		//				"\"1.315522,103.899054\"," +
		//				"\"1.320993,103.87039\"," +
		//				"\"1.392112,103.903915\"," +
		//				"\"1.448197,103.819499\"," +
		//				"\"1.352871,103.945059\"," + 
		//				"\"1.332852,103.847106\"," + 
		//				"\"1.331662,103.850483\"," +  
		//				"\"1.37171,103.894248\"," + 
		//				"\"1.312648,103.76568\"," +
		//				"\"1.346699,103.712694\"," +
		//				"\"1.292703,103.810972\"," +
		//				"\"1.37714,103.954838\"," +
		//				"\"1.357576,103.884038\"," +
		//				"\"1.333794,103.739691\"," +
		//				"\"1.339029,103.706817\"," +
		//				"\"1.416959,103.835352\"," +
		//				"\"1.284807,103.829298\"," +
		//				"\"1.385112,103.745036\"," +
		//				"\"1.394256,103.913044\"," +
		//				"\"1.318572,103.894763\"," +
		//				"\"1.379784,103.936223\"," +
		//				"\"1.32499,103.85081\"," +
		//				"\"1.286548,103.826977\"," +
		//				"\"1.435265,103.786696\"," +
		//				"\"1.429426,103.780798\"," +
		//				"\"1.372664,103.94934\"," +
		//				"\"1.397057,103.746662\"," +
		//				"\"1.289765,103.826803\"," +
		//				"\"1.354371,103.98517\"," +
		//				"\"1.381858,103.893917\"," +
		//				"\"1.323834,103.810002\"," +
		//				"\"1.321725,103.886807\"," +
		//				"\"1.323686,103.855791\"," +
		//				"\"1.432626,103.774051\"," +
		//				"\"1.297275,103.838534\"," +
		//				"\"1.274093,103.800825\"," +
		//				"\"1.302014,103.854993\"," +
		//				"\"1.45361,103.819626\"," +
		//				"\"1.320684,103.844227\"," +
		//				"\"1.276238,103.843006\"," +
		//				"\"1.355393,103.934429\"," +
		//				"\"1.335248,103.719899\"," +
		//				"\"1.437119,103.795316\"," +
		//				"\"1.332059,103.722453\"," +
		//				"\"1.429525,103.834995\"]";
		//		
		//		String redBranches = "[\"Blk 712 Ang Mo Kio Ave 6 <br />#01-4056 S(560712)\"," + 
		//				"\"Blk 166 Bukit Merah Central <br />#02-3531 S(150166)\"," + 
		//				"\"Blk 212 Bedok Nth St 1 <br />#01-147 S(460212)\"," + 
		//				"\"No.1 Jelebu Rd #01-15 & #02-11/12 <br />Bukit Panjang Plaza S(677743)\"," + 
		//				"\"Blk 510 Bishan St 13 #01-520 S(570510)\"," +
		//				"\"Blk 355 Sembawang Way #01-01 S(750355)\"," +
		//				"\"180 Kitchener Road #B1-09/10 S(208539)\"," + 
		//				"\"3 Simei St 6 #B1-04 Eastpoint S(528833)\"," + 
		//				"\"#B1-07 Hougang Mall <br />90 Hougang Ave 10 S(538766)\"," + 
		//				"\"Blk 2 Joo Chiat Road #01-1139 / #02-1139 <br />Joo Chiat Complex S(420002)\"," + 
		//				"\"Blk 71 Kallang Bahru #01-529/#02-531 S(330071)\"," +
		//				"\"11 Rivervale Crescent <br />#03-01 Rivervale Mall S(545082)\"," + 
		//				"\"30 Sembawang Drive #B1-01/02 Sun Plaza S(775713)\"," + 
		//				"\"4 Tampines Central 5 #B1-12 Tampines Mall S(529510)\"," + 
		//				"\"Blk 500 Lorong 6 Toa Payoh <br />#B1-32/#01-33 S(310500)\"," + 
		//				"\"Blk 192 Toa Payoh Lorong 4 <br />#01-670/672 S(310192)\"," + 
		//				"\"100 Hougang Ave 10 #01-01/#02-01 <br />Kang Kar Mall S(538767)\"," + 
		//				"\"Blk 451 Clementi Ave 3 #01-307 S(120451)\"," + 
		//				"\"Blk 221 Boon Lay Place #02-200 <br />Boon Lay Shopping Complex S(640221)\"," + 
		//				"\"Blk 57 Dawson Rd #01-07 Dawson Place S(142057)\"," + 
		//				"\"e! Hub Downtown East <br />1 Pasir Ris Close #02-127 S(519599)\"," +
		//				"\"Blk 202 Hougang St 21 #01-00 S(530202)\"," + 
		//				"\"Blk 135 Jurong Gateway Road <br />#01-337 S(600135)\"," + 
		//				"\"No.1 Jurong West Central 2 <br />#B1-09 Jurong Point S(648886)\"," + 
		//				"\"Blk 849 Yishun Ring Rd <br />#01-3701/#01-3703 S(760849)\"," + 
		//				"\"18/20 Kim Tian Road S(169252)\"," + 
		//				"\"21 Choa Chu Kang Ave 4 <br />#B1-03  Lot 1 Shoppers' Mall S(689812)\"," + 
		//				"\"Blk 168 Punggol Field Rd <br />Punggol Plaza #03-01/02 S(820168)\"," + 
		//				"\"10 Eunos Rd 8 #B2-13 <br />Singapore Post Centre S(408600)\"," + 
		//				"\"Blk 734 Pasir Ris St 72 #01-37 <br />Pasir Ris West Plaza S(510734)\"," + 
		//				"\"360 Balestier Rd <br />#B1-01 Shaw Plaza S(329783)\"," + 
		//				"\"302 Tiong Bahru Rd #B1-01/02 <br />Tiong Bahru Plaza S(168732)\"," + 
		//				"\"900 South Woodlands Dr #B1-01 <br />Woodlands Civic Centre S(730900)\"," + 
		//				"\"30 Woodlands Ave 1 <br />#01-11 The Woodgrove S(739065)\"," + 
		//				"\"1 Pasir Ris Central St 3 <br />#B1-10 Whitesands S(518457)\"," + 
		//				"\"21 Choa Chu Kang North 6 <br />#B1-01 S(689578)\"," + 
		//				"\"Blk 50 Havelock Rd #01-755 S(160050)\"," + 
		//				"\"65 Airport Boulevard, Basement 2 North <br />B2LS2 #B2-10 S(819663)\"," + 
		//				"\"Blk 277C Compassvale Link <br />#01-13 S(544277)\"," + 
		//				"\"587 Bukit Timah Rd #01-01 <br />Coronation Plaza S(269707)\"," + 
		//				"\"Blk 114 Aljunied Ave 2 <br />#01-75 S(380114)\"," + 
		//				"\"Blk 80 Lorong Limau <br />#01-191 S(320080)\"," + 
		//				"\"71 Woodlands Ave 3 #01-01 <br />Marsiling MRT Station S(739044)\"," + 
		//				"\"131 Killiney Rd #01-01/02/03 <br />Orchard Grand Court S(239571)\"," + 
		//				"\"460 Alexandra Road <br /> #01-09 and #01-16 S(119963)\"," + 
		//				"\"Blk 1 Rochor Rd <br />#01-640 S(180001)\"," + 
		//				"\"Blk 511 Canberra Road <br />#02-03 S(750511)\"," + 
		//				"\"10 Sinaran Dr  #04-46/47/48/49 <br />Square 2 S(307506)\"," + 
		//				"\"Blk 5 Tg Pagar Plaza <br />#01-01 S(081005)\"," + 
		//				"\"Blk 866A Tampines St. 83 <br />#01-01 S(521866)\"," + 
		//				"\"Taman Jurong Shopping Centre <br />Blk 399 Yung Sheng Rd #01-35 S(610399)\"," + 
		//				"\"Blk 888 Woodlands Dr 50 <br />#01-757 S(730888)\"," + 
		//				"\"Blk 63/66 Yung Kuang Rd <br />#01-119/#02-119 S(610063)\"," + 
		//				"\"301 Yishun Ave 2, #01-02 <br />Yishun MRT Station S(769093)\"]";
		//		
		//		// NTUC Fresh Mart
		//		String green = "[\"1.347388,103.743705\"," +
		//				"\"1.348992,103.749556\"," +
		//				"\"1.337419,103.921783\"," +
		//				"\"1.357465,103.843955\"," +
		//				"\"1.309194,103.79264\"," +
		//				"\"1.332852,103.847106\"," +
		//				"\"1.280967,103.810298\"," +
		//				"\"1.380031,103.753118\"," +
		//				"\"1.392074,103.7431\"," +
		//				"\"1.385553,103.901961\"," +
		//				"\"1.353683,103.870913\"," +
		//				"\"1.385096,103.760218\"," +
		//				"\"1.360323,103.953126\"," +
		//				"\"1.312659,103.872543\"," +
		//				"\"1.348371,103.948129\"," +
		//				"\"1.345281,103.945344\"," +
		//				"\"1.323961,103.923857\"," +
		//				"\"1.391714,103.876838\"," +
		//				"\"1.373616,103.885759\"," +
		//				"\"1.410418,103.842866\"," +
		//				"\"1.369799,103.873036\"," +
		//				"\"1.312308,103.925235\"," +
		//				"\"1.307808,103.884768\"," +
		//				"\"1.323653,103.910854\"," +
		//				"\"1.314026,103.888465\"]";
		//		
		//		String greenBranches = "[\"Blk 154A Bt Batok West Ave 8 <br />#01-01 S(651154)\"," + 
		//				"\"10 Bt Batok Central #01-08 S(659958)\"," +
		//				"\"Blk 745 Bedok Reservoir Rd <br />#01-3015 S(470745)\"," +
		//				"\"Blk 279 Bishan St 24 <br />#01-62/64 S(570279)\"," + 
		//				"\"Blk 36 Holland Drive <br />#01-03/04/05 S(270036)\"," + 
		//				"\"Blk 28 Dover Crescent <br />#01-83 S(130028)\"," + 
		//				"\"Blk 108 Depot Rd #01-01 <br />Depot Heights Shopping Centre S(100108)\"," + 
		//				"\"Blk 140 Teck Whye Lane <br />#01-351 S(680140)\"," + 
		//				"\"Blk 533 Choa Chu Kang St 51 #01-11 <br />Limbang Shopping Centre S(680533)\"," + 
		//				"\"118 Rivervale Dr #01-08 <br />Rivervale Plaza S(540118)\"," + 
		//				"\"Blk 253 Serangoon Central Drive <br />#-01-241  S(550253)\"," + 
		//				"\"Blk 628 Senja Rd #01-01 <br />Senja Grand S(670628)\"," + 
		//				"\"Blk 475 Tampines St 44 <br />#01-145 S(520475)\"," + 
		//				"\"Blk 5 Upper Boon Keng Road <br />#01-05 S(380005)\"," + 
		//				"\"Blk 107 Tampines St 11 <br />#01-353/355/357/359 S(521107)\"," + 
		//				"\"Blk 138 Tampines St 11 <br />#01-136 S(521138)\"," +
		//				"\"Blk 29B Chai Chee Avenue <br />#01-62 S(462029)\"," + 
		//				"\"21 Sengkang West Avenue <br />#01-11/12 Fernvale Point S(797650)\"," + 
		//				"\"Blk 682 Hougang Ave 4 <br />#01-310 S(530682)\"," + 
		//				"\"1 Orchid Club Road #01-34 Level 1 <br />Orchid Country Club S(769162)\"," + 
		//				"\"Blk 152B Serangoon Nth Ave 1 <br />#01-384 S(552152)\"," + 
		//				"\"934 East Coast Road <br />Siglap New Market S(459125)\"," + 
		//				"\"Blk 41 Jalan Tiga <br />#01-05 S(390041)\"," + 
		//				"\"Blk 110 Leng Kong Tiga <br />#01-233/235 S(410110)\"," + 
		//				"\"612/620 Geylang Lorong 38 <br />Singapore S(389551)\"]";
		//		
		//		// NTUC Mini Mart
		//		String yellow = "[\"1.366555,103.841631\"," +
		//				"\"1.347622,103.75715\"," +
		//				"\"1.321203,103.90355\"," +
		//				"\"1.328551,103.885349\"," +
		//				"\"1.332957,103.938221\"," +
		//				"\"1.315316,103.849454\"," +
		//				"\"1.314273,103.771361\"," +
		//				"\"1.348333,103.724307\"," +
		//				"\"1.324252,103.941572\"," +
		//				"\"1.290911,103.803121\"," +
		//				"\"1.273381,103.808981\"," +
		//				"\"1.32052,103.742652\"," +
		//				"\"1.433595,103.839687\"," +
		//				"\"1.424528,103.846596\"]";
		//		
		//		String yellowBranches = "[\"Blk 215 Ang Mo Kio Ave 1 <br />#01-877 S(560215)\"," + 
		//				"\"Blk 280 Bukit Batok East Ave 3 <br />#01-315 S(650280)\"," + 
		//				"\"Blk 5 Eunos Crescent #01-2623 S(400005)\"," + 
		//				"\"Blk 77 Circuit Road #01-464/466 S(370077)\"," + 
		//				"\"Blk 89 Bedok Nth St 4 <br />#01-77/79 S(460089)\"," + 
		//				"\"Blk 43 Cambridge Road <br />#01-15 S(210043)\"," + 
		//				"\"Blk 352 Clementi Ave 2 <br />#01-141/143 S(120352)\"," + 
		//				"\"Blk 498 Jurong West St 41 <br />#01-434/436/438 S(640498)\"," + 
		//				"\"Blk 57 New Upper Changi Rd <br />#01-1334/1336/1338 S(461057)\"," + 
		//				"\"Blk 170 Stirling Road #01-1147 S(140170)\"," + 
		//				"\"Blk 78A Telok Blangah St 32 <br />#01-01/02 S(100078)\"," + 
		//				"\"Blk 37 Teban Garden Rd <br />#01-304/305/306 S(600037)\"," + 
		//				"\"Blk 239 Yishun Ring Rd <br />#01-1150 S(760239)\"," + 
		//				"\"Blk 414 Yishun Ring Rd <br />#01-1853 S(760414)\"]";
		//		
		//		final String bluePinURL = "javascript:showBlueMarkers("+blue+","+blueBranches+");";
		//		final String orangePinURL = "javascript:showOrangeMarkers("+orange+","+orangeBranches+");";
		//		final String redPinURL = "javascript:showRedMarkers("+red+","+redBranches+");";
		//		final String greenPinURL = "javascript:showGreenMarkers("+green+","+greenBranches+");";
		//		final String yellowPinURL = "javascript:showYellowMarkers("+yellow+","+yellowBranches+");";

		webView = (WebView) findViewById(R.id.map_webview);
		webView.getSettings().setJavaScriptEnabled(true);
		//Wait for the page to load then send the location information
		webView.setWebViewClient(new WebViewClient(){
			@Override
			public void onPageFinished(WebView view, String url){
				webView.loadUrl(centerURL);
				//				webView.loadUrl(bluePinURL);
				//				webView.loadUrl(orangePinURL);
				//				webView.loadUrl(redPinURL);
				//				webView.loadUrl(greenPinURL);
				//				webView.loadUrl(yellowPinURL);
			}
		});
		webView.loadUrl(WEBVIEW_FILE_PATH);
		/** Allows JavaScript calls to access application resources **/
		//		webView.addJavascriptInterface(new JavaScriptInterface(), "android");
	}

	private void getLocation() {
		LocationManager locationManager =
				(LocationManager)getSystemService(Context.LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		String provider = locationManager.getBestProvider(criteria,true);
		//In order to make sure the device is getting the location, request updates.
		locationManager.requestLocationUpdates(provider, 1, 0, this);
		mostRecentLocation = locationManager.getLastKnownLocation(provider);
		if (mostRecentLocation == null) {
			Criteria coarse = new Criteria();
			coarse.setAccuracy(Criteria.ACCURACY_COARSE);
			provider = locationManager.getBestProvider(coarse,true);
			//In order to make sure the device is getting the location, request updates.
			locationManager.requestLocationUpdates(provider, 1, 0, this);
			mostRecentLocation = locationManager.getLastKnownLocation(provider);
		}
	}

	//	private class JavaScriptInterface {
	//		public double getLatitude(){
	//			return mostRecentLocation.getLatitude();
	//		}
	//		public double getLongitude(){
	//			return mostRecentLocation.getLongitude();
	//		}
	//	}

	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

	}

	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub

	}

	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub

	}

	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
