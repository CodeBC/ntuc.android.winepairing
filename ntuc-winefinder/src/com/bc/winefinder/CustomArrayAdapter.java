package com.bc.winefinder;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomArrayAdapter extends ArrayAdapter<String> {
	
	private final Context context;
	private final String[] texts;
	private final int[] ids;
	
	public CustomArrayAdapter(Activity context, String[] texts, int[] ids) {
		super(context, R.layout.custom_row, texts);
		this.context = context;
		this.texts = texts;
		this.ids = ids;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.custom_row, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.menu_label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.list_icon);
//		Typeface font1 = Typeface.createFromAsset(this.getContext().getApplicationContext().getAssets(), "fonts/DIN Medium.ttf");  
//	    textView.setTypeface(font1); 
		textView.setText(texts[position]);
		imageView.setImageResource(ids[position]);
		
		return rowView;
	}
}
