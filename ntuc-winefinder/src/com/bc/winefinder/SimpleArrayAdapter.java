package com.bc.winefinder;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class SimpleArrayAdapter extends ArrayAdapter<String> {
	
	private final Context context;
	private ArrayList<String> texts;
	int layoutResourceId;
	private ArrayList<String> originalTexts;
	private Filter mFilter;
	
	public SimpleArrayAdapter(Activity context, int layoutResourceId, ArrayList<String> texts) {
        super(context, layoutResourceId, texts);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.texts = texts;
        this.originalTexts = texts;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = ((Activity)context).getLayoutInflater();
		View rowView = inflater.inflate(this.layoutResourceId, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.menu_label);
		Typeface font1 = Typeface.createFromAsset(this.getContext().getApplicationContext().getAssets(), "fonts/DIN Medium.ttf");  
	    textView.setTypeface(font1); 
		textView.setText(texts.get(position));
		
		return rowView;
	}
	
	@Override
	public String getItem(int position) {
		return texts.get(position);
	}
	
	public void add(String object) {
		originalTexts.add(object);
		this.notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return texts.size();
	}
	
	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new CustomFilter();
		}
		
		return mFilter;
	}
	
	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
	}
	
	private class CustomFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			// TODO Auto-generated method stub
			FilterResults results = new FilterResults();
			if (constraint == null || constraint.length() == 0) {
				ArrayList<String> list = new ArrayList<String>(originalTexts);
				results.values = list;
				results.count = list.size();
			} else {
				ArrayList<String> newValues = new ArrayList<String>();
				for (int x = 0; x < originalTexts.size(); x++) {
					String item = originalTexts.get(x);
					if (item.contains(constraint.toString().toUpperCase())) {
						newValues.add(item);
					}
				}
				results.values = newValues;;
				results.count = newValues.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			// TODO Auto-generated method stub
			texts = (ArrayList<String>) results.values;
			notifyDataSetChanged();
		}
		
	}
}