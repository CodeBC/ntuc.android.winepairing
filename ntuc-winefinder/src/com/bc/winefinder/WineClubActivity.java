package com.bc.winefinder;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class WineClubActivity extends Activity {
	
	private ImageView wineClubCard;
	private TextView wineClubDesc;
	private Button registerButton;
	private Button emailButton;
	private TextView header;
	private Uri uriURL;
	private ProgressDialog pd;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
	    setContentView(R.layout.wine_club);
	    
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    wineClubCard = (ImageView) findViewById(R.id.wine_club_logo);
	    wineClubDesc = (TextView) findViewById(R.id.wine_club_desc);
	    registerButton = (Button) findViewById(R.id.register_btn);
	    emailButton = (Button) findViewById(R.id.email_btn);
	    registerButton.setTypeface(font2);
	    emailButton.setTypeface(font2);
	    
	    header = (TextView) findViewById(R.id.wine_club_header);
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font); 
	    
	    String wineClubDescHTML = "&#8226; Sign up and get 8% discount on wine*<br />";
	    wineClubDescHTML += "&#8226; News, updates and a personalised Just Wine Club membership card<br />";
	    wineClubDescHTML += "<p>*Exclude Japanese and Chinese wines, liquor, beer and other alcoholic drinks.</p>";
	    
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");  
	    wineClubDesc.setTypeface(font1); 
	    wineClubDesc.setText(Html.fromHtml(wineClubDescHTML));
	    
	    registerButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				uriURL = Uri.parse("https://www.fairprice.com.sg/webapp/wcs/stores/servlet/UserRegistrationForm?langId=-1&storeId=90001&catalogId=10052&new=Y&returnPage=&prevPage=&displayPromo=Y");
				Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriURL);
				startActivity(launchBrowser);
			}
	    });
	    
	    emailButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), EmailMembershipActivity.class);
					    		View v = FifthTabGroup.group.getLocalActivityManager()
					    				.startActivity("EmailMembershipActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		FifthTabGroup.group.replaceView(v);
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
	    });
	    
	    FifthTabGroup.group.changeTabWidgetOnBackButtonClick();
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
