package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class AllWinesActivity extends Activity {

	private static String webCallApi;
	private ArrayList<String> wineItemTitle;
	private ArrayList<GroupMenu> wineItem;
	private ListView wineItemList;
	private EditText searchText;
	private SimpleArrayAdapter adapter;
	private TextView header;
	private ProgressDialog pd;
	private Button filterButton;
	private int selectedPosition;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		setContentView(R.layout.all_wines);
		// initialization
		webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_id=&winetype_id=&%20wine_country=&wine_type=&wine_rating=&price_range=&all=Y";
		wineItemTitle = new ArrayList<String>();
		wineItem = new ArrayList<GroupMenu>();
		searchText = (EditText) findViewById(R.id.all_wines_search);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
		searchText.setTypeface(font1); 
		filterButton = (Button) findViewById(R.id.filter_wine);
		Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
		filterButton.setTypeface(font2); 

		// header
		header = (TextView) findViewById(R.id.all_wines_header);  
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font); 

		// add listener to check if searchText box has focus, if not then hide the keyboard
		searchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (!hasFocus) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

				}
			}
		});

		// XPath initialization
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "//item";
		InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// XPath Query
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Loop through NodeList to store data into arrays
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			NodeList childNodes = node.getChildNodes();
			String id = null;
			String title = null;
			for (int y = 0; y < childNodes.getLength(); y++) {
				if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_id")) {
					id = childNodes.item(y).getTextContent();
				} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_name")) {
					title = childNodes.item(y).getTextContent().toUpperCase();
					wineItemTitle.add(title);
				}
			}
			wineItem.add(new GroupMenu(id, title));
		}

		// Create ListView using the array of titles
		wineItemList = (ListView) findViewById(R.id.all_wines_list);
		adapter = new SimpleArrayAdapter(this, R.layout.row, wineItemTitle);
		wineItemList.setAdapter(adapter);

		ThirdTabGroup.group.changeTabWidgetOnBackButtonClick();

		// Implement search function for searcText
		searchText.addTextChangedListener(filterTextWatcher);

		// set listener
		wineItemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub

				selectedPosition = position;

				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), ViewWineDetailsActivity.class);
								newIntent.putExtra("selectedID", getItemIdByTitle(adapter.getItem(selectedPosition)));
								// Create the view using FirstTabGroup's LocalActivityManager
								View v = ThirdTabGroup.group.getLocalActivityManager()
										.startActivity("ViewWineDetailsActivity", newIntent
												.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
												.getDecorView();

								// Again, replace the view
								ThirdTabGroup.group.replaceView(v);
								// Clear searchText focus
								searchText.clearFocus();
								searchText.setText("");
								pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
		});   

	}

	public void getSearchFilter(View view) {
		// Trigger FilterActivity

		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				pd = ProgressDialog.show(getParent(), "", "Loading..");
				runOnUiThread(new Runnable(){
					public void run() {
						Intent newIntent = new Intent(getParent(), FilterActivity.class);
						// Create the view using FirstTabGroup's LocalActivityManager
						View v = ThirdTabGroup.group.getLocalActivityManager()
								.startActivity("FilterActivity", newIntent
										.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
										.getDecorView();

						// Again, replace the view
						ThirdTabGroup.group.replaceView(v);
						pd.dismiss();
					}
				}); 
				Looper.loop();
			}
		}).start();

	}

	// For Search Function
	private TextWatcher filterTextWatcher = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s); // Filter from my adapter
			adapter.notifyDataSetChanged(); // Update my view
		}

		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub

		}

	};

	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : wineItem) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}

		return null;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
