package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class ViewDishListActivity extends Activity {
	
	private static String webCallApi;
	private ArrayList<String> dishItemTitle; // this is for creating ListView
	private ArrayList<GroupMenu> dishItemArr; // Group Dish Object array
	private ListView dishItemList;
	private String selectedID;
	private Button addDish;
	private SimpleArrayAdapter adapter;
	public EditText searchText;
	public static ViewDishListActivity thisClass;
	private TextView header;
	private ProgressDialog pd;
	private int selectedPosition;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
        setContentView(R.layout.dish_list); // to be changed later
        // get the passed ID param
        selectedID = this.getIntent().getStringExtra("selectedID");
        // initialization
        webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_foodwine.php?mode=M&foodcategory_id="+selectedID;
	    dishItemTitle = new ArrayList<String>();
	    dishItemArr = new ArrayList<GroupMenu>();
	    addDish = (Button) findViewById(R.id.add_dish_btn);
	    Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    addDish.setTypeface(font2); 
	    searchText = (EditText) findViewById(R.id.dish_item_search);
	    Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    searchText.setTypeface(font1); 
	    thisClass = this;
	    
	    // header
	    header = (TextView) findViewById(R.id.dish_list_header);  
	    Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
	    header.setTypeface(font);
	    header.setText(this.getIntent().getStringExtra("dishGroupTitle"));
	    
	 // add listener to check if searchText box has focus, if not then hide the keyboard
	    searchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
	        public void onFocusChange(View v, boolean hasFocus) {
	            if (!hasFocus) {
	            	InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
	                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

	            }
	        }
	    });
	    
	    // XPath initialization
	    XPath xpath = XPathFactory.newInstance().newXPath();
	    String expression = "//item";
	    InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// XPath Query
		NodeList nodes = null;
	    try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Loop through NodeList to store data into arrays
	    for (int x = 0; x < nodes.getLength(); x++) {
	    	Node node = nodes.item(x);
	    	NodeList childNodes = node.getChildNodes();
	    	String id = null;
    		String title = null;
	    	for (int y = 0; y < childNodes.getLength(); y++) {
	    		if (childNodes.item(y).getNodeName().equalsIgnoreCase("food_id")) {
	    			id = childNodes.item(y).getTextContent();
	    		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("food_name")) {
	    			title = childNodes.item(y).getTextContent().toUpperCase();
	    			dishItemTitle.add(title);
	    		}
	    		// create the Group Dish Object and store it into the array
	    	}
	    	dishItemArr.add(new GroupMenu(id, title));
	    }
	    
	    // Create ListView using the array of titles
	    dishItemList = (ListView) findViewById(R.id.dish_list_list); // to be changed later
	    adapter = new SimpleArrayAdapter(this, R.layout.row, dishItemTitle);
	    dishItemList.setAdapter(adapter);
	    searchText.addTextChangedListener(filterTextWatcher);
        
        // set listener
	    dishItemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				// TODO Auto-generated method stub
				// Get the selected Group Dish Object
				
				selectedPosition = position;
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								// Trigger the next intent and pass the Group Dish Object ID
								Intent newIntent = new Intent(getParent(), ViewDishItemActivity.class);
								newIntent.putExtra("selectedID", getItemIdByTitle(adapter.getItem(selectedPosition)));
								newIntent.putExtra("dishTitle", adapter.getItem(selectedPosition));
								// Create the view using FirstTabGroup's LocalActivityManager
						        View v = FirstTabGroup.group.getLocalActivityManager()
						        .startActivity("ViewDishItemActivity", newIntent
						        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						        .getDecorView();

						        // Again, replace the view
						        FirstTabGroup.group.replaceView(v);
						        // Clear searchText focus
						        searchText.clearFocus();
						        searchText.setText("");
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
			}
		});
        
	    // set listener for add dish button
	    addDish.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				
				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), AddDishFormActivity.class);
								// Create the view using FirstTabGroup's LocalActivityManager
						        View v = FirstTabGroup.group.getLocalActivityManager()
						        .startActivity("AddDishFormActivity", newIntent
						        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						        .getDecorView();

						        // Again, replace the view
						        FirstTabGroup.group.replaceView(v);
						        // Clear searchText focus
						        searchText.clearFocus();
						        searchText.setText("");
						        pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
				
			} 	
	    });
    }
	
	// For Search Function
	private TextWatcher filterTextWatcher = new TextWatcher() {

		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// do nothing
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s); // Filter from my adapter
			adapter.notifyDataSetChanged(); // Update my view
		}

		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub

		}
	};
	
	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : dishItemArr) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}
		
		return null;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
