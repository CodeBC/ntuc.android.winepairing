package com.bc.winefinder;

import java.util.ArrayList;

import com.bc.winefinder.SecondTabGroup.SecondTabOnBackButtonClick;
import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;


import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;

public class ThirdTabGroup extends ActivityGroup implements SwapMethod {

	// Keep this in a static variable to make it accessible for all the nested activities, lets them manipulate the view
	public static ThirdTabGroup group;

	// Need to keep track of the history if you want the back-button to work properly, don't use this if your activities requires a lot of memory.
	private ArrayList<View> history;
	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		this.history = new ArrayList<View>();
		group = this;

		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				pd = ProgressDialog.show(getParent(), "", "Loading..");
				runOnUiThread(new Runnable(){
					public void run() {
						// Start the root activity within the group and get its view
						View view = getLocalActivityManager().startActivity("AllWinesActivity", new
								Intent(ThirdTabGroup.this,AllWinesActivity.class)
						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
						.getDecorView();

						// Replace the view of this ActivityGroup
						replaceView(view);
						changeTabWidgetOnBackButtonClick();
						pd.dismiss();
					}
				}); 
				Looper.loop();
			}
		}).start();

	}

	public void replaceView(View v) {
		// Adds the old one to history
		history.add(v);
		// Changes this Groups View to the new View.
		setContentView(v);
	}

	public void back() {

		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				pd = ProgressDialog.show(getParent(), "", "Loading..");
				runOnUiThread(new Runnable(){
					public void run() {
						if(history.size() > 1) {
							history.remove(history.size()-1);
							setContentView(history.get(history.size()-1));
						}else {
							View v = getParent().getWindow().getDecorView();
							Intent newIntent = new Intent(v.getContext(), MainActivity.class);
							startActivity(newIntent);
							finish();
						}
						pd.dismiss();
					}
				}); 
				Looper.loop();
			}
		}).start();
	}

	public class ThirdTabOnBackButtonClick implements SwapMethod {

		public void onBackButtonClick() {
			// TODO Auto-generated method stub
			ThirdTabGroup.group.back();
			return;
		}
	}

	public void changeTabWidgetOnBackButtonClick() {
		TabWidget.changeOnBackButtonClickMethod(new ThirdTabOnBackButtonClick());
	}

	public void onBackButtonClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBackPressed() {
		back();
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}