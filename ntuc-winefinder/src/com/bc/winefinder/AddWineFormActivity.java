package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

public class AddWineFormActivity extends Activity {

	private TextView nameLabel;
	private TextView emailLabel;
	private TextView wineLabel;
	private TextView headerInfo;
	private EditText name;
	private EditText email;
	private EditText wine;
	private TextView header;
	private Button submit;
	private String nameValue;
	private String emailValue;
	private String wineValue;
	private String webCallApi;
	private ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
		
		setContentView(R.layout.add_wine_form);

		// header
		header = (TextView) findViewById(R.id.add_wine_header);  
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font);

		nameLabel = (TextView) findViewById(R.id.add_wine_name_label);
		emailLabel = (TextView) findViewById(R.id.add_wine_email_label);
		wineLabel = (TextView) findViewById(R.id.add_wine_wine_label);
		headerInfo = (TextView) findViewById(R.id.add_wine_header_info);
		name = (EditText) findViewById(R.id.add_wine_name);
		email = (EditText) findViewById(R.id.add_wine_email);
		wine = (EditText) findViewById(R.id.add_wine_wine);

		submit = (Button) findViewById(R.id.add_wine_submit);
		Typeface font1 = Typeface.createFromAsset(getAssets(),"fonts/DIN Medium.ttf");
		submit.setTypeface(font1);

		Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");
		headerInfo.setTypeface(font2);

		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								// submit the request through API call and show pop up and then end the activity
								nameValue = name.getText().toString().replace(" ", "+");
								emailValue = email.getText().toString();
								wineValue = wine.getText().toString().replace(" ", "+");

								if (nameValue.equalsIgnoreCase("") || emailValue.equalsIgnoreCase("") || 
										wineValue.equalsIgnoreCase("")) {
									pd.dismiss();
									Toast.makeText(AddWineFormActivity.this, "Please fill in the required fields", Toast.LENGTH_LONG).show();
								} else {
									// validate email first
									if (!isValidEmail(emailValue)) {
										pd.dismiss();
										Toast.makeText(AddWineFormActivity.this, "Invalid email address, please try again.", Toast.LENGTH_LONG).show();
									} else {
										webCallApi = "https://www.balancedfbapps.com/ntuc/modules/suggestion/_insert_suggestwine.php?suggestwine_name="+nameValue+"&suggestwine_email="+emailValue+"&suggestwine_wine="+wineValue+"&suggestwine_type=W&mode=M";
										// make the call and pop up window	    

										String message = null;

										// XPath initialization
										XPath xpath = XPathFactory.newInstance().newXPath();
										String expression = "//Suggestion";
										InputSource inputSource = null;
										try {
											inputSource = new InputSource(new URL(webCallApi).openStream());
										} catch (MalformedURLException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										// XPath Query
										NodeList nodes = null;
										try {
											nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
										} catch (XPathExpressionException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

										// Loop through NodeList to store data into arrays
										for (int x = 0; x < nodes.getLength(); x++) {
											Node node = nodes.item(x);
											NodeList childNodes = node.getChildNodes();
											for (int y = 0; y < childNodes.getLength(); y++) {
												if (childNodes.item(y).getNodeName().equalsIgnoreCase("message")) {
													message = childNodes.item(y).getTextContent();
												}
											}
										}

										pd.dismiss();
										Toast.makeText(AddWineFormActivity.this, message, Toast.LENGTH_LONG).show();
										InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
										imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
									}
								}
							}
						}); 
						Looper.loop();
					}
				}).start();

			}

		});
	}

	private Boolean isValidEmail(String inputEmail) {
		String regExpn = 
				"^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
						+"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
						+"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
						+"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		Pattern patternObj = Pattern.compile(regExpn);

		Matcher matcherObj = patternObj.matcher(inputEmail/*May be fetched from  EditText. This string is the one which you wanna validate for email*/);
		if (matcherObj.matches()) {
			//Valid email id�
			return true;
		} else {
			//not a valid email id�
			return false;
		}
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
