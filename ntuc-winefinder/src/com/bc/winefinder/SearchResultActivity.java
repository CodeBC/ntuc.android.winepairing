package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SearchResultActivity extends Activity {

	private static String webCallApi;
	private ArrayList<String> wineList;
	private ListView resultList;
	private String type;
	private String country;
	private String price;
	private String rating;
	private TextView header;
	private ArrayList<GroupMenu> wineItem;
	private SimpleArrayAdapter adapter;
	private int selectedPosition;
	private ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		setContentView(R.layout.filter_result);
		type = this.getIntent().getStringExtra("selectedType").length() > 0 ? this.getIntent().getStringExtra("selectedType").substring(0, 1) : this.getIntent().getStringExtra("selectedType");
		country = this.getIntent().getStringExtra("selectedCountry").replace(" ", "+");
		rating = this.getIntent().getStringExtra("selectedRating");
		price = this.getIntent().getStringExtra("selectedPrice");
		wineItem = new ArrayList<GroupMenu>();

		// header
		header = (TextView) findViewById(R.id.filter_result_header);  
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font); 

		// initialization
		webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_country="+country+"&wine_type="+type+"&wine_rating="+rating+"&price_range="+price+"&all=Y";
		wineList = new ArrayList<String>();

		// XPath initialization
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "//item";
		InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// XPath Query
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Loop through NodeList to store data into arrays
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			NodeList childNodes = node.getChildNodes();
			String id = null;
			String title = null;
			for (int y = 0; y < childNodes.getLength(); y++) {
				if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_id")) {
					id = childNodes.item(y).getTextContent();
				} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_name")) {
					title = childNodes.item(y).getTextContent().toUpperCase();
					wineList.add(title);
				}
			}
			wineItem.add(new GroupMenu(id, title));
		}


		resultList = (ListView) findViewById(R.id.filter_result_list);
		// handle no result
		if (wineList.size() < 1) {
			wineList.add("There is no record.");
			adapter = new SimpleArrayAdapter(this, R.layout.row, wineList);
			resultList.setAdapter(adapter);
		} else {
			// Create ListView using the array of titles
			adapter = new SimpleArrayAdapter(this, R.layout.row, wineList);
			resultList.setAdapter(adapter);

			// set listener
			resultList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

				public void onItemClick(AdapterView<?> arg0, View view, int position,
						long arg3) {
					// TODO Auto-generated method stub
					selectedPosition = position;

					new Thread(new Runnable() {
						public void run() {
							Looper.prepare();
							pd = ProgressDialog.show(getParent(), "", "Loading..");
							runOnUiThread(new Runnable(){
								public void run() {
									// Trigger the next intent and pass the Group Dish Object ID
									Intent newIntent = new Intent(getParent(), ViewWineDetailsActivity.class);
									newIntent.putExtra("selectedID", getItemIdByTitle(adapter.getItem(selectedPosition)));
									// Create the view using FirstTabGroup's LocalActivityManager
									View v = ThirdTabGroup.group.getLocalActivityManager()
											.startActivity("ViewWineDetailsActivity", newIntent
													.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
													.getDecorView();

									// Again, replace the view
									ThirdTabGroup.group.replaceView(v);
									pd.dismiss();
								}
							}); 
							Looper.loop();
						}
					}).start();

				}
			});
		}

	}

	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : wineItem) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}

		return null;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
