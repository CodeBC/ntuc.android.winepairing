package com.bc.winefinder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class PairFoodActivity extends Activity {
	
	private static String webCallApi;
	private ArrayList<String> groupDishTitle; // this is for creating ListView
	private ArrayList<String> groupDishIcon;
	private ArrayList<GroupMenu> groupDishArr; // Group Dish Object array
	private ListView groupDishList;
	private ExtendedArrayAdapter adapter;
	private EditText searchText;
	private TextView header;
	private ProgressDialog pd;
	private int selectedPosition;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
		
        setContentView(R.layout.pair_food);
        
        webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_food_category.php?mode=M";
        groupDishTitle = new ArrayList<String>();
        groupDishIcon = new ArrayList<String>();
        groupDishArr = new ArrayList<GroupMenu>();
        searchText = (EditText) findViewById(R.id.dish_group_search);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
	    searchText.setTypeface(font1); 

        // header
        header = (TextView) findViewById(R.id.pair_food_header);  
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
        header.setTypeface(font); 

        // add listener to check if searchText box has focus, if not then hide the keyboard
        searchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
        	public void onFocusChange(View v, boolean hasFocus) {
        		if (!hasFocus) {
        			InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        			imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

        		}
        	}
        });

        // XPath initialization
        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = "//item";
        InputSource inputSource = null;
        try {
        	inputSource = new InputSource(new URL(webCallApi).openStream());
        } catch (MalformedURLException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        } catch (IOException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }

        // XPath Query
        NodeList nodes = null;
        try {
        	nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }

        // Loop through NodeList to store data into arrays
        for (int x = 0; x < nodes.getLength(); x++) {
        	Node node = nodes.item(x);
        	NodeList childNodes = node.getChildNodes();
        	String id = null;
        	String title = null;
        	String icon = null;
        	for (int y = 0; y < childNodes.getLength(); y++) {
        		if (childNodes.item(y).getNodeName().equalsIgnoreCase("foodcategory_id")) {
        			id = childNodes.item(y).getTextContent();
        		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("foodcategory_name")) {
        			title = childNodes.item(y).getTextContent().toUpperCase();
        			groupDishTitle.add(title);
        		} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("foodcategory_image")) {
        			icon = childNodes.item(y).getTextContent();
        			groupDishIcon.add(icon);
        		}
        		// create the Group Dish Object and store it into the array
        	}
        	groupDishArr.add(new GroupMenu(id, title, icon));
        }
        
        ArrayList<Drawable> icons = getIconDrawables(groupDishIcon);

        // Create ListView using the array of titles
        groupDishList = (ListView) findViewById(R.id.pair_food_list);
        adapter = new ExtendedArrayAdapter(PairFoodActivity.this, groupDishTitle, icons);
        groupDishList.setAdapter(adapter);
        
        searchText.addTextChangedListener(filterTextWatcher); 
        FirstTabGroup.group.changeTabWidgetOnBackButtonClick();

	    // set listener
	    groupDishList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

	    	public void onItemClick(AdapterView<?> arg0, View view, int position,
	    			long arg3) {
	    		// TODO Auto-generated method stub
	    		// Trigger the next intent and pass the Group Dish Object ID
	    		
	    		selectedPosition = position;
	    		
	    		new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								Intent newIntent = new Intent(getParent(), ViewDishListActivity.class);
					    		newIntent.putExtra("selectedID", getItemIdByTitle(adapter.getItem(selectedPosition)));
					    		newIntent.putExtra("dishGroupTitle", adapter.getItem(selectedPosition));
					    		// Create the view using FirstTabGroup's LocalActivityManager
					    		View v = FirstTabGroup.group.getLocalActivityManager()
					    				.startActivity("ViewDishListActivity", newIntent
					    						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
					    						.getDecorView();

					    		// Again, replace the view
					    		FirstTabGroup.group.replaceView(v);
					    		// Clear searchText focus
					    		searchText.clearFocus();
					    		searchText.setText("");
								pd.dismiss();
							}
						}); 
						Looper.loop();
					}
				}).start();
	    		
	    	}
	    });
    }
	
	// For Search Function
	private TextWatcher filterTextWatcher = new TextWatcher() {
		
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			// do nothing
			
		}

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			adapter.getFilter().filter(s); // Filter from my adapter
		    adapter.notifyDataSetChanged(); // Update my view
		    
		}

		public void afterTextChanged(Editable arg0) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : groupDishArr) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}
		
		return null;
	}
	
	private ArrayList<Drawable> getIconDrawables(ArrayList<String> iconURL) {
		ArrayList<Drawable> result = new ArrayList<Drawable>();
		for (int x = 0; x < iconURL.size(); x++) {
			URL url = null;
			HttpURLConnection connection = null;
			InputStream is = null;
			try {
				url = new URL("https://www.balancedfbapps.com/ntuc/images/"+iconURL.get(x));
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				connection = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
			try {
				is = connection.getInputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Bitmap img = BitmapFactory.decodeStream(is);
			result.add(new BitmapDrawable(Bitmap.createScaledBitmap(img, 75, 50, true)));
		}
		
		return result;
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}
