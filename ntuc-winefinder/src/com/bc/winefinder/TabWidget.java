package com.bc.winefinder;

import java.lang.reflect.Method;
import java.util.ArrayList;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

public class TabWidget extends TabActivity implements SwapMethod {

	private int selectedTab;
	private static Button backButton;
	private ImageView fairPriceLogo;
	private Button sharingButton;
	private Button orderButton;
	private TabHost tabHost;
	public ProgressDialog pd;
	public static TabWidget group;
	private Typeface font;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);
        
        setContentView(R.layout.tab_widget);
        group = this;
        
        pd = ProgressDialog.show(this, "", "Loading..", true);
        selectedTab = this.getIntent().getIntExtra("selectedTab", 0);
        backButton = (Button) findViewById(R.id.back_btn);
        fairPriceLogo = (ImageView) findViewById(R.id.fair_price_logo);
        sharingButton = (Button) findViewById(R.id.sharing_btn);
        orderButton = (Button) findViewById(R.id.order_btn);
        font = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");  
        
        backButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBackButtonClick(v);
			}
        });
        
        sharingButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tabHost.setCurrentTab(6);
			}
		});
        
        orderButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tabHost.setCurrentTab(5);
			}
		});
        
        new Thread() { public void run() {
        	TabWidget.this.runOnUiThread(new Runnable() {
        		@Override
        		public void run() {
        			try {
        				tabHost = getTabHost();  // The activity TabHost
        				TabHost.TabSpec spec;  // Reusable TabSpec for each tab
        				Intent intent;  // Reusable Intent for each tab
        				Drawable icon;

        				// Pair Food Tab #0
        				intent = new Intent().setClass(getApplicationContext(), FirstTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("pairFood");  
        				spec.setContent(intent);  
        				icon = getResources().getDrawable(R.drawable.pair_food_icon);
        				spec.setIndicator("PAIR FOOD", icon);
        				tabHost.addTab(spec);

        				// Pair Wine Tab #1
        				intent = new Intent().setClass(getApplicationContext(), SecondTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("pairWine");  
        				spec.setContent(intent);  
        				icon = getResources().getDrawable(R.drawable.pair_wine_icon);
        				spec.setIndicator("PAIR WINE", icon);  
        				tabHost.addTab(spec);

        				// All Wines Tab #2
        				intent = new Intent().setClass(getApplicationContext(), ThirdTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("allWines");  
        				spec.setContent(intent);  
        				icon = getResources().getDrawable(R.drawable.all_wines_icon);
        				spec.setIndicator("ALL WINES", icon);  
        				tabHost.addTab(spec);

        				// Tips & Videos Tab #3
        				intent = new Intent().setClass(getApplicationContext(), FourthTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("tipsVideos");  
        				spec.setContent(intent);  
        				icon = getResources().getDrawable(R.drawable.tips_videos_icon);
        				spec.setIndicator("TIPS & VIDEOS", icon);  
        				tabHost.addTab(spec);

        				// Just Wine Club Tab #4
        				intent = new Intent().setClass(getApplicationContext(), FifthTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("wineClub");  
        				spec.setContent(intent);  
        				icon = getResources().getDrawable(R.drawable.wine_club_icon);
        				spec.setIndicator("JUST WINE CLUB", icon);  
        				tabHost.addTab(spec);   

        				// For Order Wine, tab #5
        				intent = new Intent().setClass(getApplicationContext(), SixthTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("orderWine");  
        				spec.setContent(intent);  
        				spec.setIndicator("Order Wine");
        				tabHost.addTab(spec);

        				// For Sharing, tab #6
        				intent = new Intent().setClass(getApplicationContext(), SeventhTabGroup.class);
        				// Initialize a TabSpec for each tab and add it to the TabHost
        				spec = tabHost.newTabSpec("sharing");  
        				spec.setContent(intent);  
        				spec.setIndicator("Sharing");
        				tabHost.addTab(spec);

        				// Hide both #5 and #6
        				getTabWidget().getChildAt(getTabWidget().getChildCount()-1).setVisibility(View.GONE);
        				if (Integer.valueOf(android.os.Build.VERSION.SDK) == 10) {
        					getTabWidget().getChildAt(getTabWidget().getChildCount()-3).setVisibility(View.GONE);
        				} else {
        					getTabWidget().getChildAt(getTabWidget().getChildCount()-2).setVisibility(View.GONE);
        				}

        				tabHost.getTabWidget().setStripEnabled(false);

        				// set tab background
        				for (int x = 0; x < tabHost.getTabWidget().getChildCount(); x++) {
        					tabHost.getTabWidget().getChildAt(x).setBackgroundColor(Color.parseColor("#000000"));
        				}

        				tabHost.setCurrentTab(selectedTab);
        				getTabHost().setOnTabChangedListener(new OnTabChangeListener() {
        					public void onTabChanged(String tabId) {
        						InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        						imm.hideSoftInputFromWindow(tabHost.getWindowToken(), 0);
        						int i = getTabHost().getCurrentTab();
        						switch(i) {
        						case 0:
        							FirstTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						case 1:
        							SecondTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						case 2:
        							ThirdTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						case 3:
        							FourthTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						case 4:
        							FifthTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						case 5:
        							SixthTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						case 6:
        							SeventhTabGroup.group.changeTabWidgetOnBackButtonClick();
        							break;
        						}
        					}
        				});
        			} catch (Exception e) {
        				Log.e("THREAD ERROR", e.getMessage());
        			}
        			pd.dismiss();
        		}
        	});
        }
        }.start();
    }
	
	public void onBackButtonClick(View view) {
		Intent newIntent = new Intent(view.getContext(), MainActivity.class);
		startActivity(newIntent);
		finish();
	}
	
	public static void changeOnBackButtonClickMethod(final SwapMethod sm) {
		backButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				sm.onBackButtonClick();
			}
        });
	}

	public void onBackButtonClick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}