package com.bc.winefinder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.bc.winefinder.util.Constants;
import com.inmobi.adtracker.androidsdk.IMAdTracker;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil;
import com.inmobi.adtracker.androidsdk.IMAdTrackerUtil.LOG_LEVEL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OrderFormActivity extends Activity {


	private EditText firstName;
	private EditText surName;
	private EditText email;
	private EditText mobile;
	private EditText address;
	private EditText clubNumber;
	private Button submit;
	private TextView header;
	private TextView clubInfo;
	private String eFirstName;
	private String eSurName;
	private String eEmail;
	private String eMobile;
	private String eAddress;
	private String eClubNumber;
	private Spinner wineSelector1, wineSelector2, wineSelector3, wineSelector4, wineSelector5;
	private Spinner totalBottle1, totalBottle2, totalBottle3, totalBottle4, totalBottle5;
	private ArrayList<String> totalBottleArr;
	private ArrayList<String> wineList;
	private static String webCallApi;
	private ArrayList<GroupMenu> wineListArr;
	private ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		//Tracker Initiated
		IMAdTrackerUtil.setLogLevel(LOG_LEVEL.VERBOSE);
		IMAdTracker.getInstance().startSession(getApplicationContext(), Constants.inmobi_id);

		View viewToLoad = LayoutInflater.from(this.getParent()).inflate(R.layout.order_form, null);
		setContentView(viewToLoad);

		Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/DIN RegularAlternate.ttf");
		Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/DIN Medium.ttf");

		firstName = (EditText) findViewById(R.id.order_firstname);
		firstName.setTypeface(font1);
		surName = (EditText) findViewById(R.id.order_surname);
		surName.setTypeface(font1);
		email = (EditText) findViewById(R.id.order_email);
		email.setTypeface(font1);
		mobile = (EditText) findViewById(R.id.order_mobile);
		mobile.setTypeface(font1);
		address = (EditText) findViewById(R.id.order_address);
		address.setTypeface(font1);
		clubNumber = (EditText) findViewById(R.id.order_club_number);
		clubNumber.setTypeface(font1);
		submit = (Button) findViewById(R.id.submit_btn);
		submit.setTypeface(font2);

		clubInfo = (TextView) findViewById(R.id.club_member_info);
		clubInfo.setTypeface(font1);
		clubInfo.setText(Html.fromHtml("<font color='white'>[save 8%, <br />not a member?]</font>"), TextView.BufferType.SPANNABLE);
		clubInfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				TabWidget.group.getTabHost().setCurrentTab(4);
			}
		});

		wineSelector1 = (Spinner) findViewById(R.id.order_wine_list1);
		wineSelector2 = (Spinner) findViewById(R.id.order_wine_list2);
		wineSelector3 = (Spinner) findViewById(R.id.order_wine_list3);
		wineSelector4 = (Spinner) findViewById(R.id.order_wine_list4);
		wineSelector5 = (Spinner) findViewById(R.id.order_wine_list5);
		totalBottle1 = (Spinner) findViewById(R.id.order_total_bottle1);
		totalBottle2 = (Spinner) findViewById(R.id.order_total_bottle2);
		totalBottle3 = (Spinner) findViewById(R.id.order_total_bottle3);
		totalBottle4 = (Spinner) findViewById(R.id.order_total_bottle4);
		totalBottle5 = (Spinner) findViewById(R.id.order_total_bottle5);
		totalBottleArr = new ArrayList<String>();
		wineList = new ArrayList<String>();
		wineListArr = new ArrayList<GroupMenu>();

		for (int x = 0; x <= 20; x++) {
			if (x == 0)
				totalBottleArr.add("No of case");
			else	
				totalBottleArr.add(String.valueOf(x));
		}

		// All
		webCallApi = "https://www.balancedfbapps.com/ntuc/modules/foodwine/_read_wine.php?mode=M&wine_type=&all=Y";
		// wine product list
		// XPath initialization
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "//item";
		InputSource inputSource = null;
		try {
			inputSource = new InputSource(new URL(webCallApi).openStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// XPath Query
		NodeList nodes = null;
		try {
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Loop through NodeList to store data into arrays
		wineList.add("SELECT A WINE");
		for (int x = 0; x < nodes.getLength(); x++) {
			Node node = nodes.item(x);
			NodeList childNodes = node.getChildNodes();
			String id = null;
			String title = null;
			for (int y = 0; y < childNodes.getLength(); y++) {
				if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_id")) {
					id = childNodes.item(y).getTextContent();
				} else if (childNodes.item(y).getNodeName().equalsIgnoreCase("wine_name")) {
					title = childNodes.item(y).getTextContent().toUpperCase();
					wineList.add(title);
				}
			}
			wineListArr.add(new GroupMenu(id, title));

		}

		ArrayAdapter<String> wineListAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, wineList);
		wineListAdapter1.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		wineSelector1.setAdapter(wineListAdapter1);

		ArrayAdapter<String> wineListAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, wineList);
		wineListAdapter2.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		wineSelector2.setAdapter(wineListAdapter2);

		ArrayAdapter<String> wineListAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, wineList);
		wineListAdapter3.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		wineSelector3.setAdapter(wineListAdapter3);

		ArrayAdapter<String> wineListAdapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, wineList);
		wineListAdapter4.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		wineSelector4.setAdapter(wineListAdapter4);

		ArrayAdapter<String> wineListAdapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, wineList);
		wineListAdapter5.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		wineSelector5.setAdapter(wineListAdapter5);

		ArrayAdapter<String> totalBottleAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, totalBottleArr);
		totalBottleAdapter1.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		totalBottle1.setAdapter(totalBottleAdapter1);

		ArrayAdapter<String> totalBottleAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, totalBottleArr);
		totalBottleAdapter2.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		totalBottle2.setAdapter(totalBottleAdapter2);

		ArrayAdapter<String> totalBottleAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, totalBottleArr);
		totalBottleAdapter3.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		totalBottle3.setAdapter(totalBottleAdapter3);

		ArrayAdapter<String> totalBottleAdapter4 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, totalBottleArr);
		totalBottleAdapter4.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		totalBottle4.setAdapter(totalBottleAdapter4);

		ArrayAdapter<String> totalBottleAdapter5 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, totalBottleArr);
		totalBottleAdapter5.setDropDownViewResource(R.layout.multiline_spinner_dropdown_item);
		totalBottle5.setAdapter(totalBottleAdapter5);

		//header 
		header = (TextView) findViewById(R.id.order_form_header);
		Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Tommaso.ttf");  
		header.setTypeface(font); 

		submit.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				// TODO Auto-generated method stub

				new Thread(new Runnable() {
					public void run() {
						Looper.prepare();
						pd = ProgressDialog.show(getParent(), "", "Loading..");
						runOnUiThread(new Runnable(){
							public void run() {
								eFirstName = firstName.getText().toString();
								eSurName = surName.getText().toString();
								eEmail = email.getText().toString();
								eMobile = mobile.getText().toString();
								eAddress = address.getText().toString();
								eClubNumber = clubNumber.getText().toString();
								int tmp_total1 = 0;
								int tmp_total2 = 0;
								int tmp_total3 = 0;
								int tmp_total4 = 0;
								int tmp_total5 = 0;

								if (!String.valueOf(wineSelector1.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
									tmp_total1 = String.valueOf(totalBottle1.getSelectedItem()).length() > 5 ? 0 : Integer.parseInt(String.valueOf(totalBottle1.getSelectedItem()));
									if (!String.valueOf(wineSelector2.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
										tmp_total2 = String.valueOf(totalBottle2.getSelectedItem()).length() > 5 ? 0 : Integer.parseInt(String.valueOf(totalBottle2.getSelectedItem()));
										if (!String.valueOf(wineSelector3.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
											tmp_total3 = String.valueOf(totalBottle3.getSelectedItem()).length() > 5 ? 0 : Integer.parseInt(String.valueOf(totalBottle3.getSelectedItem()));
											if (!String.valueOf(wineSelector4.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
												tmp_total4 = String.valueOf(totalBottle4.getSelectedItem()).length() > 5 ? 0 : Integer.parseInt(String.valueOf(totalBottle4.getSelectedItem()));
												if (!String.valueOf(wineSelector5.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
													tmp_total5 = String.valueOf(totalBottle5.getSelectedItem()).length() > 5 ? 0 : Integer.parseInt(String.valueOf(totalBottle5.getSelectedItem()));

													int total = tmp_total1 + tmp_total2 + tmp_total3 + tmp_total4 + tmp_total5;

													if (eFirstName.equalsIgnoreCase("") || eSurName.equalsIgnoreCase("") || eEmail.equalsIgnoreCase("") ||
															eMobile.equalsIgnoreCase("") || eAddress.equalsIgnoreCase("")) {
														pd.dismiss();
														Toast.makeText(OrderFormActivity.this, "Please fill in the required fields", Toast.LENGTH_LONG).show();
													} else if (total < 5) {
														pd.dismiss();
														Toast.makeText(OrderFormActivity.this, "Minimum order is 5 cases", Toast.LENGTH_LONG).show();
													} else {
														// validate email first 
														if (!isValidEmail(eEmail)) {
															pd.dismiss();
															Toast.makeText(OrderFormActivity.this, "Invalid email address, please try again.", Toast.LENGTH_LONG).show();
														} else {
															Intent newIntent = new Intent(getParent(), OrderConfirmationActivity.class);
															newIntent.putExtra("firstName", eFirstName);
															newIntent.putExtra("surName", eSurName);
															newIntent.putExtra("email", eEmail);
															newIntent.putExtra("mobile", eMobile);
															newIntent.putExtra("address", eAddress); 
															newIntent.putExtra("clubNumber", eClubNumber);

															// when only a wine is selected, send the ID and name
															if (tmp_total1 > 0 && !String.valueOf(wineSelector1.getSelectedItem()).equalsIgnoreCase("SELECT A WINE")) 
																newIntent.putExtra("wineOrder1", String.valueOf(wineSelector1.getSelectedItem()));
															newIntent.putExtra("selectedID1", getItemIdByTitle(String.valueOf(wineSelector1.getSelectedItem())));
															newIntent.putExtra("total1", String.valueOf(tmp_total1));
															if (tmp_total2 > 0 && !String.valueOf(wineSelector2.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
																newIntent.putExtra("wineOrder2", String.valueOf(wineSelector2.getSelectedItem()));
															newIntent.putExtra("selectedID2", getItemIdByTitle(String.valueOf(wineSelector2.getSelectedItem())));
															newIntent.putExtra("total2", String.valueOf(tmp_total2));
															if (tmp_total3 > 0 && !String.valueOf(wineSelector3.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
																newIntent.putExtra("wineOrder3", String.valueOf(wineSelector3.getSelectedItem()));
															newIntent.putExtra("selectedID3", getItemIdByTitle(String.valueOf(wineSelector3.getSelectedItem())));
															newIntent.putExtra("total3", String.valueOf(tmp_total3));
															if (tmp_total4 > 0 && !String.valueOf(wineSelector4.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
																newIntent.putExtra("wineOrder4", String.valueOf(wineSelector4.getSelectedItem()));
															newIntent.putExtra("selectedID4", getItemIdByTitle(String.valueOf(wineSelector4.getSelectedItem())));
															newIntent.putExtra("total4", String.valueOf(tmp_total4));
															if (tmp_total5 > 0 && !String.valueOf(wineSelector5.getSelectedItem()).equalsIgnoreCase("SELECT A WINE"))
																newIntent.putExtra("wineOrder5", String.valueOf(wineSelector5.getSelectedItem()));
															newIntent.putExtra("selectedID5", getItemIdByTitle(String.valueOf(wineSelector5.getSelectedItem())));
															newIntent.putExtra("total5", String.valueOf(tmp_total5));

															View v = SixthTabGroup.group.getLocalActivityManager()
																	.startActivity("OrderConfirmationActivity", newIntent
																			.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
																			.getDecorView();

															// Again, replace the view
															SixthTabGroup.group.replaceView(v);
															pd.dismiss();
														}
													}
							}
						}); 
						Looper.loop();
					}
				}).start();

			}
		});

	}

	private String getItemIdByTitle(String title) {
		for (GroupMenu gm : wineListArr) {
			if (gm.getTitle().equalsIgnoreCase(title)) {
				return gm.getId();
			}
		}

		return null;
	}

	private Boolean isValidEmail(String inputEmail) {
		String regExpn = 
				"^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
						+"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
						+"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
						+"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
						+"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		Pattern patternObj = Pattern.compile(regExpn);

		Matcher matcherObj = patternObj.matcher(inputEmail/*May be fetched from  EditText. This string is the one which you wanna validate for email*/);
		if (matcherObj.matches()) {
			//Valid email id�
			return true;
		} else {
			//not a valid email id�
			return false;
		}
	}

	@Override
	public void onDestroy()
	{
		IMAdTracker.getInstance().stopSession();
		super.onDestroy();
	}
	@Override
	public void onPause()
	{
		IMAdTracker.getInstance().stopSession();
		super.onPause();
	}
}